using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Linq;
using Chester.Configuration;

namespace Chester.Modules.Public_Commands
{
    public class PlayerCommands : ModuleBase
    {
        /*
        [Command("add")]
        public async Task Add(IUser user)
        {
            var result = Database.CheckExistingAdmin(user);
            if (result.Count <= 0)
            {
                
                //Database.EnterUserMainTable(user);
                //Database.EnterUserNoteTable(user);
                //Database.EnterUserUserdataTable(user);
                Database.EnterUserAdminTable(user);
                await Context.Message.Author.SendMessageAsync("added message.");
                Console.Write($"{ DateTime.Now} [REPORTING DATABASE] " + user.Username + " has been added to the database.\n");
            }
            else
            {
                await Context.Message.Author.SendMessageAsync("");
                Console.Write($"{ DateTime.Now} [REPORTING DATABASE] " + user.Username + " already in the database.\n");
            }
        }
        
        [Command("I agree")]
        [Summary("By typing this, you agree to the terms and conditions.")]
        public async Task Agree()
        {
            var user = Context.User;
            var result = Database.CheckExistingUser(user);
            if (result.Count <= 0)
            {
                await Context.Message.Author.SendMessageAsync("I'm sorry but you must -add.");
                return;
            }
            var player = user as SocketGuildUser;
            var n = Database.GetUserStatus(player);
            var m = Database.GetUserNotes(player);
            if ((n.FirstOrDefault().UserId == m.FirstOrDefault().UserId) && n.FirstOrDefault().Agree != 1)
            {
                var gld = Context.Guild as SocketGuild;
                var role = gld.Roles.Where(has => has.Name.ToUpper() == "player - first class".ToUpper());
                var roleRemove = gld.Roles.Where(has => has.Name.ToUpper() == "applicant".ToUpper());
                await Context.Channel.SendMessageAsync("You have agreed to the terms and conditions set from by the staff.\n" +
                    "To get started with commands please type: **-help**");
                Database.AgreeToRules(user, 1);
                Database.UpdateRole(user, 1);
                await player.AddRolesAsync(role);
                await player.RemoveRolesAsync(roleRemove);
            }
            else
            {
                await Context.Channel.SendMessageAsync("You did not type -I agree yourName \n" +
                                            "To join the server as a regular member you must first agree to the Terms and Conditions, and type the agree command in the #new member channel.\n");
            }
        }
        [Command("award gold")]
        public async Task Award(SocketGuildUser user, [Remainder] int gold)
        {
            var GuildUser = await Context.Guild.GetUserAsync(Context.User.Id);
            if (!(GuildUser.Id == 109406914869235712))
            {
                return;
            }
            else
            {
                Database.ChangeGold(user, gold);
                await Context.Message.Author.SendMessageAsync($"{user.Username} was awarded: " + gold + " gold!");
            }
        }
        [Command("award exp")]
        public async Task Exp(SocketGuildUser user, [Remainder] int xp)
        {
            var GuildUser = await Context.Guild.GetUserAsync(Context.User.Id);
            if (!(GuildUser.Id == 109406914869235712))
            {
                return;
            }
            else
            {
                Database.ChangeXp(user, xp);
                await Context.Message.Author.SendMessageAsync($"{user.Username} was awarded " + xp + " experience!");
            }
        }
        */
    }
}
