using System;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Discord.Commands;
using System.Text;
using System.Diagnostics;
using System.Linq;
using Chester.Configuration;
using System.Collections.Generic;

namespace Chester.Modules.Public_Commands
{
    [Group("admin")]
    public class AdminCommands : ModuleBase
    {
        /*
         * Here's where we can add in our channel IDs and color codes to use later.
         * TODO: Anything else that can be ref here?
         */
        private static readonly Color Blue = Color.Blue;//Info
        private static readonly Color Gold = Color.Gold;//Admin general
        private static readonly Color Green = Color.Green;//Admin good
        private static readonly Color Red = Color.Red;//Admin bad
        private static readonly Color Purple = Color.Purple;//Admin help
        private static readonly ulong CHANNEL = 567074661179981834;//Maxwell's channel on SWGP
        private static readonly ulong ServerStatus = 476570664711618560;//This is SWGP and below
        private static readonly ulong Announcements = 476593939957153794;
        private static readonly ulong Population = 482937419667210250;
        private static readonly ulong ProjectRules = 476593860500520960;
        private static readonly ulong FridayFeatures = 487792942115848219;
        private static readonly ulong NewMembers = 476596183939416065;
        private static readonly ulong General = 476622219460739073;
        private static readonly ulong ServerDiscussion = 476623181528956928;
        private static readonly ulong Cantina = 476623647230918656;
        private static readonly ulong Screenshots = 476624560960176149;
        private static readonly ulong TopG = 563404046036369428;
        private static readonly ulong PlayerAnnouncements = 483951869081681931;
        private static readonly ulong PlayerGuides = 496008636892708874;
        private static readonly ulong CommunitySuggestions = 477924914322472971;
        private static readonly ulong CityGuildRecruiting = 476623952454615041;
        private static readonly ulong TesterArmy = 477989070677475338;
        private static readonly ulong Lfg = 476624100983308298;
        private static readonly ulong Trade = 476624247893262336;
        private static readonly ulong MedicEntertainerBuffers = 480445497669058562;
        private static readonly ulong Resources = 476624295645413387;
        private static readonly ulong Crafting = 476624382660313088;
        private static readonly ulong Pvp = 476624417854849026;
        private static readonly ulong BeastMaster = 487771427672555560;
        private static readonly ulong Roleplay = 476624486859538447;
        private static readonly ulong Space = 476624514130640896;
        private static readonly ulong Gcw = 476624542064705536;
        private static readonly ulong ForumFeed = 478530535518502912;
        private static readonly ulong StaffAnnouncements = 476611520042631168;
        private static readonly ulong StaffCommands = 519353740474449954;
        private static readonly ulong Staff = 476611902106238997;
        private static readonly ulong CommunityStaff = 480746606141112361;
        private static readonly ulong DevelopmentStaff = 476612192440156171;
        private static readonly ulong QaDiscussion = 477988655437185034;
        private static readonly ulong EventDevelopment = 476613045498675212;
        private static readonly ulong StaffTraining = 476612869643960332;
        private static readonly ulong ModeratorActions = 476616090873757696;
        private static readonly ulong Consultants = 476613242714718208;
        private static readonly ulong QualityOfLife = 480029280961560589;
        private static readonly ulong StaffActions = 538570049968144394;
        private static readonly ulong InactivePlayer = 476628886705274880;
        private static readonly ulong SecuredPlayer = 482015094570614804;
        private static readonly ulong PenaltyBox = 542797538151890948;
        private static readonly ulong InternalAffairs = 476645214141022208;
        private static readonly ulong Admin = 476791409655939085;
        private static readonly ulong BotProgramming = 477856240785948672;
        private static readonly ulong CaptainRex = 546845562214023209;
        private static readonly ulong TeamLeaders = 501263563269079040;
        private static readonly string NoPerm = "You don't have permission to use this command.";
        //This command is the default, and will display all current commands.
        [Command]
        public async Task DefaultAdminCommand()
        {
            var task = Task.Run(async () =>
            {
                var admin = Context.User;
                var tableName = Database.GetAdminStatus(admin);
                var mKey = tableName.FirstOrDefault().MasterKey;
                if (mKey < 1)
                {
                    await Context.User.SendMessageAsync(NoPerm);
                    return;
                }
                else
                {
                    var embed = new EmbedBuilder();
                    embed.WithColor(Gold);
                    embed.WithTitle("**Admin Command List**");
                    embed.WithDescription("Below is a list of all the current commands:\n " +
                                    "**IF** you need help with any of the commands, please look at this example:\n" +
                                    "-admin adduser help\n\n" +
                                    "**IF** a user has a **SPACE** in their name, you **HAVE** to use their Id.\n" +
                                    "To obtain their Id, please use: -admin viewuser UserName\n\n\n" +
                                    "-admin adduser username\n" +
                                    "-admin viewuser username\n" +
                                    "-admin setrole roleName username notes\n" +
                                    "-admin addadmin username\n" +
                                    "-admin viewadmin username\n" +
                                    "-admin kick username notes\n" +
                                    "-admin ban username notes\n" +
                                    "-admin note username notes\n" +
                                    "-admin info\n" +
                                    "-admin message channelName yourMessage\n" +
                                    "-admin botaccess 1/2/3/0 username **(This command requires level 3 access)**\n" +
                                    "-admin setforum username forumName\n" +
                                    "-admin removeadmin username\n");
                    await Context.User.SendMessageAsync("", false, embed.Build());
                }
            });
        }
        //This command will allow the admin to send a message to whatever channel they want from the bot.
        [Command("message")]
        public async Task AdminMaxwellChat(string chan, [Remainder] string str)
        {
            var task = Task.Run(async () =>
            {
                var dictionary = new Dictionary<string, ulong>();
                dictionary.Add("general", General);
                dictionary.Add("announcements", Announcements);
                dictionary.Add("serverdiscussion", ServerDiscussion);
                dictionary.Add("population", Population);
                dictionary.Add("projectrules", ProjectRules);
                dictionary.Add("fridayfeatures", FridayFeatures);
                dictionary.Add("newmembers", NewMembers);
                dictionary.Add("cantina", Cantina);
                dictionary.Add("screenshots", Screenshots);
                dictionary.Add("topg", TopG);
                dictionary.Add("playerannouncements", PlayerAnnouncements);
                dictionary.Add("playerguides", PlayerGuides);
                dictionary.Add("communitysuggestions", CommunitySuggestions);
                dictionary.Add("cityguildrecruiting", CityGuildRecruiting);
                dictionary.Add("testerarmy", TesterArmy);
                dictionary.Add("lfg", Lfg);
                dictionary.Add("trade", Trade);
                dictionary.Add("medicentertainerbuffers", MedicEntertainerBuffers);
                dictionary.Add("resources", Resources);
                dictionary.Add("crafting", Crafting);
                dictionary.Add("pvp", Pvp);
                dictionary.Add("beastmaster", BeastMaster);
                dictionary.Add("roleplay", Roleplay);
                dictionary.Add("space", Space);
                dictionary.Add("gcw", Gcw);
                dictionary.Add("forumfeed", ForumFeed);
                dictionary.Add("staffannouncements", StaffAnnouncements);
                dictionary.Add("staffcommands", StaffCommands);
                dictionary.Add("staff", Staff);
                dictionary.Add("communitystaff", CommunityStaff);
                dictionary.Add("developmentstaff", DevelopmentStaff);
                dictionary.Add("qastaff", QaDiscussion);
                dictionary.Add("eventdevelopment", EventDevelopment);
                dictionary.Add("stafftraining", StaffTraining);
                dictionary.Add("moderatoractions", ModeratorActions);
                dictionary.Add("consultants", Consultants);
                dictionary.Add("qualityoflife", QualityOfLife);
                dictionary.Add("staffactions", StaffActions);
                dictionary.Add("serverstatus", ServerStatus);
                var admin = Context.User;
                var tableName = Database.GetAdminStatus(admin);
                var mKey = tableName.FirstOrDefault().MasterKey;
                string listOfChannels = "";
                var list = new List<string>(dictionary.Keys);
                foreach (string key in list) { listOfChannels += "\n" + key; }
                string title = "**Your message will contain**";
                var embed = new EmbedBuilder();
                embed.WithColor(Gold);
                if (mKey < 1)
                {
                    await Context.Message.Author.SendMessageAsync(NoPerm);
                    return;
                }
                else
                {
                    title = "**Message from the staff**";
                    if (dictionary.ContainsKey(chan))
                    {
                        ulong value = dictionary[chan];
                        var test = await Context.Guild.GetChannelAsync(value) as SocketTextChannel;
                        embed.WithTitle(title);
                        embed.WithDescription(str);
                        await test.SendMessageAsync("", false, embed.Build());
                    }
                    else
                    {
                        embed.WithTitle("**You didn't input a correct channel!**");
                        embed.WithDescription("You didn't pick a proper channel. Here's a current list of the channels you can use: " + listOfChannels);
                        await Context.User.SendMessageAsync("", false, embed.Build());
                    }

                }
            });
        }
        //Adding a user to the database
        [Command("setforum")]
        public async Task AdminAddForum(string forum, [Remainder] IUser user)
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            var result = Database.CheckExistingUser(user);

            var task = Task.Run(async () =>
            {
                if (mKey < 1)
                {
                    await chan.SendMessageAsync(NoPerm);
                    return;
                }
                if (result.Count <= 0)
                {
                    await chan.SendMessageAsync("**" + admin.Username + "**, this user is not in the database. You need to add the user to the database by using the command -admin adduser " + user.Username);
                }
                else
                {
                    Database.ChangeForumName(user, forum);
                    var embed = new EmbedBuilder();
                    embed.WithColor(Green);
                    embed.WithTitle($"**UPDATE FORUM NAME TO DATABASE**");
                    embed.WithDescription($"User information:\n" +
                                        $"User ID: " + user.Id + "\n" +
                                         "Username: " + user + "\n" +
                                         "Forum name: " + forum + "\n" +
                                         "You are updating their forum name to the database.\n" +
                                         "Admin who added forum name: " + Context.User.Username);
                    await chan.SendMessageAsync("", false, embed.Build());
                }
            });
        }
        //Adding a user to the database
        [Command("adduser")]
        public async Task AdminAddUser([Remainder] IUser user)
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            var result = Database.CheckExistingUser(user);

            var task = Task.Run(async () =>
            {
                if (mKey < 1)
                {
                    await chan.SendMessageAsync(NoPerm);
                    return;
                }
                if (result.Count <= 0)
                {
                    Database.EnterUserMainTable(user);
                    Database.EnterUserUserdataTable(user);
                    Database.EnterUserNoteTable(user);
                    Database.WhoAddedWho(user, admin);
                    Database.ChangeLastAdd(admin);
                    var embed = new EmbedBuilder();
                    embed.WithColor(Green);
                    embed.WithTitle($"**ADD USER TO DATABASE**");
                    embed.WithDescription($"User information:\n" +
                                        $"User ID: " + user.Id + "\n" +
                                         "Username: " + user + "\n" +
                                         "You are adding this user to the database.\n" +
                                         "Admin who added: " + Context.User.Username);
                    await chan.SendMessageAsync("", false, embed.Build());
                }
                else
                {
                    await chan.SendMessageAsync("**" + admin.Username + "**, this user is already in the database. To view this user's data use the command -admin viewuser " + user.Username);
                }
            });
        }
        //This command is the basic allow or deny for the bot commands
        [Command("BotAccess")]
        public async Task AdminGiveKey(string access, [Remainder] IUser user)
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            var result = Database.CheckExistingAdmin(user);
            string key = "";
            int value = 0;
            var lowAcc = access.ToLower();

            var task = Task.Run(async () =>
            {
                if (mKey > 3)
                {
                    await chan.SendMessageAsync(NoPerm + "\n**You need level 3 permissions to use this command**");
                    return;
                }
                switch (lowAcc)
                {
                    case "1":
                        key = "Has been given level 1 access.";
                        value = 1;
                        break;
                    case "2":
                        key = "Has been given level 2 access.";
                        value = 2;
                        break;
                    case "3":
                        key = "Has been given level 3 access.";
                        value = 3;
                        break;
                    default:
                        key = "**YOU HAVE REMOVED ACCESS**";
                        break;

                }
                if (result.Count <= 0)
                {
                    await chan.SendMessageAsync("**" + admin.Username + "**, this admin is not in the database. You need to -admin addadmin " + user.Username);
                }
                else
                {
                    Database.ChangeMasterKey(user, value);
                    var embed = new EmbedBuilder();
                    embed.WithColor(Gold);
                    embed.WithTitle($"**MODIFING ADMIN ACCESS TO BOT**");
                    embed.WithDescription($"User information:\n" +
                                        $"User ID: " + user.Id + "\n" +
                                         "Username: " + user + "\n" +
                                         "You are changing this admin's access to the bot.\n" +
                                         "Admin who modified access: " + Context.User.Username + "\n" +
                                         "Admin Access: " + key);
                    await chan.SendMessageAsync("", false, embed.Build());
                }
            });
        }
        //This command allows the admin to view the users account
        [Command("viewuser")]
        public async Task AdminViewUser([Remainder] IUser user)
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            string message = "**" + admin.Username + "**, this user is not in the database. Try using -admin adduser " + user.Username;

            var task = Task.Run(async () =>
            {
                if (mKey < 1)
                {
                    await chan.SendMessageAsync(NoPerm);
                    return;
                }
                var result = Database.CheckExistingUser(user);
                var result2 = Database.CheckExistingUserdata(user);
                var result3 = Database.CheckExistingNote(user);
                if (result.Count <= 0 || result2.Count <= 0 || result3.Count <= 0)
                {
                    await chan.SendMessageAsync(message);
                    return;
                }
                else
                {
                    var main = Database.GetMainStatus(user);
                    var note = Database.GetNoteStatus(user);
                    var data = Database.GetUserdataStatus(user);
                    var uid = main.FirstOrDefault().UserId;
                    var un = main.FirstOrDefault().UserName;
                    var fn = main.FirstOrDefault().ForumName;
                    var wa = main.FirstOrDefault().WhoAdded;
                    DateTime jd = main.FirstOrDefault().JoinDate;
                    var kickNote = note.FirstOrDefault().KickNotes;
                    var banNote = note.FirstOrDefault().BanNotes;
                    var promoteNote = note.FirstOrDefault().PromoteNotes;
                    var generalNote = note.FirstOrDefault().GeneralNotes;
                    var timeoutNote = note.FirstOrDefault().TimeoutNotes;
                    DateTime wk = data.FirstOrDefault().WhenKick;
                    DateTime wb = data.FirstOrDefault().WhenBan;
                    DateTime wt = data.FirstOrDefault().WhenTimeout;
                    DateTime wp = data.FirstOrDefault().WhenPromoted;
                    string wnm = data.FirstOrDefault().WhoNoteMe;
                    string wrm = data.FirstOrDefault().WhoRoleMe;
                    string wtm = data.FirstOrDefault().WhoTimeoutMe;
                    string wkm = data.FirstOrDefault().WhoKickMe;
                    string wbm = data.FirstOrDefault().WhoBanMe;
                    string kick = "**This user hasn't been kicked before.**\n";
                    string ban = "**This user hasn't been banned before.**\n";
                    string time = "**This user hasn't been put in timeout before.**\n";
                    string notes = "**This user has no notes on their account.**\n";
                    string role = "**This user hasn't had their role modified.**\n";
                    string space = "\n\n";
                    if (wnm != "none")
                    {
                        notes = "**Admin who NOTE:** " + wnm + "\n**Admin's notes:**\n" + generalNote + "\n\n";
                    }
                    if (wrm != "maxwell" && wrm != "none")
                    {
                        role = "**Admin who SETROLE:** " + wrm + "\n**When:** " + wp.ToString() + "\n**Admin's notes:**\n" + promoteNote + "\n\n";
                    }
                    if (wtm != "none")
                    {
                        time = "**Admin who put user in TIMEOUT:** " + wtm + "\n**When:** " + wt.ToString() + "\n**Admin's notes:**\n" + timeoutNote + "\n\n";
                    }
                    if (wkm != "none")
                    {
                        kick = "**Admin who KICKED user:** " + wkm + "\n**When:** " + wk.ToString() + "\n**Admin's notes:**\n" + kickNote + "\n\n";
                    }
                    if (wbm != "none")
                    {
                        ban = "**Admin who BANNED user:** " + wbm + "\n**When:** " + wb.ToString() + "\n**Admin's notes:**\n" + banNote + "\n\n";
                    }
                    var embed = new EmbedBuilder();
                    embed.WithColor(Blue);
                    embed.WithTitle($"**VIEW USER INFO**");
                    embed.WithDescription($"**User information:**\n" +
                                        $"**User ID:** " + uid + "\n" +
                                         "**User name:** " + un + "\n" +
                                         "**Forum name:** " + fn + "\n" +
                                         "**Join Date:** " + jd.ToString() + "." +
                                         space + notes +
                                         space + role +
                                         space + time +
                                         space + kick +
                                         space + ban);
                    await chan.SendMessageAsync("", false, embed.Build());
                }
            });
        }
        //this command allows the admin to modify their role
        [Command("setrole")]
        public async Task SetRole(string role, IUser user, [Remainder] string notes)
        {
            var admin = Context.User;
            var roler = user as SocketGuildUser;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            var result = Database.CheckExistingUserdata(user);
            var gld = Context.Guild as SocketGuild;
            string player = "acive player";
            string inactivePlayer = "inactive player";
            string securedPlayer = "secured player";
            var task = Task.Run(async () =>
            {
                if (mKey < 1)
                {
                    await chan.SendMessageAsync(NoPerm);
                    return;
                }
                if (result.Count <= 0)
                {
                    await chan.SendMessageAsync("**" + admin.Username + "**, this user is not in the database. Issue the -admin adduser userNameOrUserId");
                }
                else
                {

                    var rem1 = gld.Roles.Where(has => has.Name.ToUpper() == player.ToUpper());
                    var rem2 = gld.Roles.Where(has => has.Name.ToUpper() == inactivePlayer.ToUpper());
                    var rem3 = gld.Roles.Where(has => has.Name.ToUpper() == securedPlayer.ToUpper());
                    switch (role)
                    {
                        case "player":
                            role = player;
                            await roler.RemoveRolesAsync(rem2);
                            await roler.RemoveRolesAsync(rem3);
                            break;
                        case "inacive":
                            role = inactivePlayer;
                            await roler.RemoveRolesAsync(rem1);
                            await roler.RemoveRolesAsync(rem3);
                            break;
                        case "secured":
                            role = securedPlayer;
                            await roler.RemoveRolesAsync(rem1);
                            await roler.RemoveRolesAsync(rem2);
                            break;
                        default:
                            await chan.SendMessageAsync("**" + admin.Username + "**, you didn't enter a correct role ID. Please refer to -admin setrole help.\n");
                            break;
                    }
                    Database.ChangeUserRole(user, role);
                    Database.WhoRoleMe(user, admin);
                    Database.ChangeLastRole(admin);
                    Database.UpdateNotePromote(user, notes);
                    Database.WhenPromoted(user);
                    Database.WhoRoleMe(user, admin);
                    var embed = new EmbedBuilder();
                    embed.WithColor(Gold);
                    embed.WithTitle($"**SETTING USER ROLE**");
                    embed.WithDescription($"User information:\n" +
                                         "Username: " + user.Username + " is getting their role set to : " + role + ".\n" +
                                         "your notes: " + notes);
                    await chan.SendMessageAsync("", false, embed.Build());
                    var real = gld.Roles.Where(has => has.Name.ToUpper() == role.ToUpper());
                    await roler.AddRolesAsync(real);
                }
            });
        }
        //This command allows the admin to put a user in timeout for a duration
        [Command("timeout")]
        public async Task SetRole(IUser user, int dur, string time, [Remainder] string notes)
        {
            var admin = Context.User;
            var roler = user as SocketGuildUser;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            var result = Database.CheckExistingUserdata(user);
            var gld = Context.Guild as SocketGuild;
            string member = "active player";
            string timeout = "timeout cage";

            if (mKey < 1)
            {
                await chan.SendMessageAsync(NoPerm);
                return;
            }
            if (result.Count <= 0)
            {
                await chan.SendMessageAsync("**" + admin.Username + "**, this user is not in the database. Try using -admin adduser " + user.Username);
            }
            else
            {

                var rem1 = gld.Roles.Where(has => has.Name.ToUpper() == member.ToUpper());
                var rem2 = gld.Roles.Where(has => has.Name.ToUpper() == timeout.ToUpper());
                await roler.RemoveRolesAsync(rem1);
                await roler.AddRolesAsync(rem2);
                Database.WhenPromoted(user);
                Database.WhoTimeoutMe(user, admin);
                var embed = new EmbedBuilder();
                embed.WithColor(Red);
                embed.WithTitle($"**SETTING USER IN TIMEOUT**");
                embed.WithDescription("User information:\n" +
                                     "Username: " + user.Username + " is getting their role set to : timeout!.\n" +
                                     "your notes: " + notes);
                await chan.SendMessageAsync("", false, embed.Build());
                var task = Task.Run(async () =>
                {
                    int newDur = dur;
                    dur = dur * 1000;
                    int sec = dur;
                    int min = dur * 60;
                    int hour = dur * 3600;
                    string s = "";
                    string type = "";
                    switch (time)
                    {
                        case "hours":
                        case "hour":
                        case "hrs":
                        case "hr":
                            dur = hour;
                            if (hour > (3600 * 1000))
                            {
                                s = "s";
                            }
                            type = " Hour" + s;
                            break;
                        case "minutes":
                        case "minute":
                        case "min":
                        case "m":
                            dur = min;
                            if (min > (60 * 1000))
                            {
                                s = "s";
                            }
                            type = " Minute" + s;
                            break;
                        default:
                            dur = sec;
                            if (sec > 1000)
                            {
                                s = "s";
                            }
                            type = " Second" + s;
                            break;
                    }
                    await user.SendMessageAsync("You've been put into timeout for a duration of: " + newDur + type + ".\n");
                    await Task.Delay(dur);
                    await roler.RemoveRolesAsync(rem2);
                    await roler.AddRolesAsync(rem1);
                });
            }
        }
        //This command allows the admin to add another admin to the database
        [Command("addadmin")]
        public async Task AdminAddAdmin([Remainder] IUser user)
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            var result = Database.CheckExistingAdmin(user);

            var task = Task.Run(async () =>
            {
                if (mKey < 1)
                {
                    await chan.SendMessageAsync(NoPerm);
                    return;
                }
                if (result.Count <= 0)
                {
                    Database.EnterUserAdminTable(user);
                    Database.ChangeLastAddAdmin(admin);
                    var embed = new EmbedBuilder();
                    embed.WithColor(Green);
                    embed.WithTitle($"**ADD ADMIN TO THE ADMIN DATABASE**");
                    embed.WithDescription($"User information:\n" +
                                        $"User ID: " + user.Id + "\n" +
                                         "Username: " + user + "\n" +
                                         "You are adding this user to the database.\n" +
                                         "Admin who added: " + Context.User.Username);
                    await chan.SendMessageAsync("", false, embed.Build());
                }
                else
                {
                    await chan.SendMessageAsync("**" + admin.Username + "**, this admin is already in the admin database. To view this admin's data use: -admin viewadmin " + user.Username);
                }
            });
        }
        //This command allows the admin to remove another admin from the database
        [Command("removeadmin")]
        public async Task AdminRemoveAdmin([Remainder] IUser user)
        {
            Console.WriteLine("Command called");
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            var result = Database.CheckExistingAdmin(user);

            var task = Task.Run(async () =>
            {
                Console.WriteLine("Started task.");
                if (mKey < 2)
                {
                    Console.WriteLine("You don't have perms");
                    await chan.SendMessageAsync(NoPerm);
                    return;
                }
                if (result.Count >= 0)
                {
                    Console.WriteLine("About to check db.");
                    Database.RemoveUserAdminTable(user);
                    var embed = new EmbedBuilder();
                    embed.WithColor(Green);
                    embed.WithTitle($"**REMOVE ADMIN FROM THE ADMIN DATABASE**");
                    embed.WithDescription($"User information:\n" +
                                        $"User ID: " + user.Id + "\n" +
                                         "Username: " + user + "\n" +
                                         "You are removing this user from the database.\n" +
                                         "Admin who removed: " + Context.User.Username);
                    await chan.SendMessageAsync("", false, embed.Build());
                }
                else
                {
                    await chan.SendMessageAsync("**" + admin.Username + "**, this admin is not in the admin database.");
                }
            });
        }
        //This command allows the admin to view another admins past actions
        [Command("viewadmin")]
        public async Task AdminViewAdmin([Remainder] IUser user)
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            var task = Task.Run(async () =>
            {
                if (mKey < 1)
                {
                    await chan.SendMessageAsync(NoPerm);
                    return;
                }
                var result = Database.CheckExistingAdmin(user);
                if (result.Count <= 0)
                {
                    await chan.SendMessageAsync("There is no admin by that name in the database. To add: -admin addadmin " + user.Username);
                    return;
                }
                else
                {
                    var main = Database.GetAdminStatus(user);
                    var when = Database.GetAdminWhenStatus(user);
                    var uid = main.FirstOrDefault().UserId;
                    var un = main.FirstOrDefault().AdminName;
                    var wr = when.FirstOrDefault().LastRole;
                    var wa = when.FirstOrDefault().LastAdd;
                    var waa = when.FirstOrDefault().LastAddAdmin;
                    var wad = when.FirstOrDefault().WhenAdded;
                    var wk = when.FirstOrDefault().LastKick;
                    var wb = when.FirstOrDefault().LastBan;
                    var wn = when.FirstOrDefault().LastNote;
                    string whenRole = "Haven't used setrole.\n";
                    string whenAdd = "Haven't used adduser.\n";
                    string whenAddAdmin = "Haven't used addadmin.\n";
                    string whenKick = "Haven't used kick.\n";
                    string whenBan = "Haven't used ban.\n";
                    string whenNote = "Hasn't been noted.\n";
                    if (wr.ToString() != wad.ToString())
                    {
                        whenRole = wr.ToString();
                    }
                    if (wa.ToString() != wad.ToString())
                    {
                        whenAdd = wa.ToString();
                    }
                    if (waa.ToString() != wad.ToString())
                    {
                        whenAddAdmin = waa.ToString();
                    }
                    if (wk.ToString() != wad.ToString())
                    {
                        whenKick = wk.ToString();
                    }
                    if (wb.ToString() != wad.ToString())
                    {
                        whenBan = wb.ToString();
                    }
                    if (wn.ToString() != wad.ToString())
                    {
                        whenNote = wn.ToString();
                    }
                    var embed = new EmbedBuilder();
                    embed.WithColor(Blue);
                    embed.WithTitle("**CHECK ADMIN IN ADMIN DATABASE**");
                    embed.WithDescription($"**Admin information:**\n" +
                                        $"Admin ID: " + uid + "\n" +
                                         "Admin name: " + un + "\n" +
                                         "setrole: " + whenRole + "\n" +
                                         "adduser: " + whenAdd + "\n" +
                                         "addadmin: " + whenAddAdmin + "\n" +
                                         "kick: " + whenKick + "\n" +
                                         "ban: " + whenBan + "\n" +
                                         "note: " + whenNote + "\n");
                    await chan.SendMessageAsync("", false, embed.Build());
                }
            });
        }
        //This allows the admin to kick a user from the discord
        [Command("kick")]
        //TODO ENABLE KICK CHECK, AND KICK FUNCTION UPON ACTIVATION
        [RequireUserPermission(GuildPermission.KickMembers)]
        //[RequireBotPermission(GuildPermission.KickMembers)]
        public async Task KickUser(IUser user, [Remainder] string msg)
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            var task = Task.Run(async () =>
            {
                if (mKey < 1)
                {
                    await chan.SendMessageAsync(NoPerm);
                    return;
                }
                else
                {
                    var userr = user as SocketGuildUser;
                    await userr.KickAsync();
                    Database.ChangeLastKick(admin);
                    Database.UpdateNoteKick(user, msg);
                    Database.WhenKick(user);
                    Database.WhoKickMe(user, admin);
                    var embed = new EmbedBuilder();
                    embed.WithColor(Red);
                    embed.WithTitle($"**Kick User Confirmation Page**");
                    embed.WithDescription("User information:\n" +
                                    $"User ID: " + Context.User.Id + "\n" +
                                     "Username: " + user + "\n" +
                                     "You are **KICKING** this user for: \n" + msg);
                    await chan.SendMessageAsync("", false, embed.Build());
                }
            });
        }
        //This command allows the admin to ban a user from the discord
        [Command("ban")]
        [RequireUserPermission(GuildPermission.Administrator)]
        //[RequireBotPermission(GuildPermission.BanMembers)]
        public async Task BanUser(IUser user, [Remainder] string msg)
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            var task = Task.Run(async () =>
            {
                if (mKey < 1)
                {
                    await chan.SendMessageAsync(NoPerm);
                    return;
                }
                else
                {
                    var userr = user as SocketGuildUser;
                    var gld = Context.Guild as SocketGuild;
                    await gld.AddBanAsync(user);
                    Database.ChangeLastBan(user);
                    Database.UpdateNoteBan(user, msg);
                    Database.WhenBan(user);
                    Database.WhoBanMe(user, admin);
                    var embed = new EmbedBuilder();
                    embed.WithColor(Red);
                    embed.WithTitle("**Ban User Confirmation Page**");
                    embed.WithDescription("User information:\n" +
                                    $"User ID: " + Context.User.Id + "\n" +
                                     "Username: " + user + "\n" +
                                     "You are **BANNING** this user for: \n" + msg);
                    await chan.SendMessageAsync("", false, embed.Build());
                }
            });
        }
        //This command allows the admin to put general notes on the users account
        [Command("note")]
        public async Task NoteUser(IUser user, [Remainder] string msg)
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            var task = Task.Run(async () =>
            {
                if (mKey < 1)
                {
                    await chan.SendMessageAsync(NoPerm);
                    return;
                }
                else
                {
                    var userr = user as SocketGuildUser;
                    var gld = Context.Guild as SocketGuild;
                    Database.ChangeLastNote(user);
                    Database.UpdateNoteGeneral(user, msg);
                    Database.WhoNoteMe(user, admin);
                    var embed = new EmbedBuilder();
                    embed.WithColor(Gold);
                    embed.WithTitle("**Note User Confirmation Page**");
                    embed.WithDescription("User information:\n" +
                                    $"User ID: " + Context.User.Id + "\n" +
                                     "Username: " + user + "\n" +
                                     "You are **NOTE** this user: \n" + msg);
                    await chan.SendMessageAsync("", false, embed.Build());
                }
            });
        }
        //This command gives the user general information about the discord server itself
        [Command("info")]
        public async Task Info()
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            var task = Task.Run(async () =>
            {
                if (mKey < 1)
                {
                    await chan.SendMessageAsync(NoPerm);
                    return;
                }
                else
                {
                    var embed = new EmbedBuilder();
                    embed.WithColor(Blue);
                    var gld = Context.Guild as SocketGuild;
                    var client = Context.Client as DiscordSocketClient;
                    var O = gld.Owner.Username;
                    var V = gld.VoiceRegionId;
                    var C = gld.CreatedAt;
                    var N = gld.DefaultMessageNotifications;
                    var VL = gld.VerificationLevel;
                    var XD = gld.Roles.Count;
                    var X = gld.MemberCount;
                    var Z = client.ConnectionState;
                    embed.WithTitle(gld.Name + "Server Information");
                    embed.WithDescription("Server Owner: **" + O + "\n**Voice Region: **" + V + "\n**Created At: **" + C + "\n**MsgNtfc: **" + N + "\n**Verification: **" + VL + "\n**Role Count: **" + XD + "\n **Members: **" + X + "\n **Conntection state: **" + Z + "\n\n**");
                    await Context.Message.Author.SendMessageAsync("", false, embed.Build());
                }
            });
        }
        //Only skyyyr can modify the game the bot is playing
        [Command("SetGame")]
        [Summary("Sets a 'Game' for the bot")]
        public async Task setgame([Remainder] string game)
        {
            var GuildUser = await Context.Guild.GetUserAsync(Context.User.Id);
            bool access = false;
            if ((GuildUser.Id == 109406914869235712) || (GuildUser.Id == 186223102747738114))
            {
                access = true;
            }
            
            if (access == false)
            {
                await Context.Channel.SendMessageAsync(NoPerm);
                return;
            }
            else
            {
                await (Context.Client as DiscordSocketClient).SetGameAsync(game);
                await Context.Channel.SendMessageAsync($"Successfully Set the game to *{game}*");
                Console.WriteLine($"{DateTime.Now}: Game was changed to {game}");
            }
        }
        //This allows the user to get the bots information
        [Command("Botinfo")]
        [Summary("Shows All Bot Info.")]
        public async Task BotInfo()
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            var task = Task.Run(async () =>
            {
                if (mKey < 1)
                {
                    await chan.SendMessageAsync(NoPerm);
                    return;
                }
                else
                {
                    using (var process = Process.GetCurrentProcess())
                    {
                        var embed = new EmbedBuilder();
                        var application = await Context.Client.GetApplicationInfoAsync();
                        embed.ImageUrl = application.IconUrl;
                        embed.WithColor(Blue)

                        .AddField(y =>
                        {
                            y.Name = "Author.";
                            y.Value = application.Owner.Username; application.Owner.Id.ToString();
                            y.IsInline = false;
                        })
                        .AddField(y =>
                        {
                            y.Name = "Uptime.";
                            var time = DateTime.Now - process.StartTime;
                            var sb = new StringBuilder();
                            if (time.Days > 0)
                            {
                                sb.Append($"{time.Days}d ");
                            }
                            if (time.Hours > 0)
                            {
                                sb.Append($"{time.Hours}h ");
                            }
                            if (time.Minutes > 0)
                            {
                                sb.Append($"{time.Minutes}m ");
                            }
                            sb.Append($"{time.Seconds}s ");
                            y.Value = sb.ToString();
                            y.IsInline = true;
                        })
                        .AddField(y =>
                        {
                            y.Name = "Discord.net version.";
                            y.Value = DiscordConfig.Version;
                            y.IsInline = true;
                        }).AddField(y =>
                        {
                            y.Name = "Server Amount.";
                            y.Value = (Context.Client as DiscordSocketClient).Guilds.Count.ToString();
                            y.IsInline = false;
                        })
                        .AddField(y =>
                        {
                            y.Name = "Number Of Users";
                            y.Value = (Context.Client as DiscordSocketClient).Guilds.Sum(g => g.Users.Count).ToString();
                            y.IsInline = false;
                        })
                        .AddField(y =>
                        {
                            y.Name = "Channels";
                            y.Value = (Context.Client as DiscordSocketClient).Guilds.Sum(g => g.Channels.Count).ToString();
                            y.IsInline = false;
                        });
                        await Context.Message.Author.SendMessageAsync("", false, embed.Build());
                    }
                }
            });
        }


        /*
         * HELP COMMAND SECTION
         */

        //Help command for adding a user to the database
        [Command("setforum help")]
        public async Task AdminSetForumHelp()
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            if (mKey < 1)
            {
                await chan.SendMessageAsync(NoPerm);
                return;
            }
            else
            {
                var embed = new EmbedBuilder();
                embed.WithColor(Purple);
                embed.WithTitle($"**SETFORUM HELP**");
                embed.WithDescription("Here is the information you requested **" + admin.Username + "**.\n" +
                                            "-admin setforum forumName userName\n **EXAMPLE**: -admin setforum myForumName skyyyr\n");
                await chan.SendMessageAsync("", false, embed.Build());
            }
        }
        //Help command for adding a user to the database
        [Command("adduser help")]
        public async Task AdminAddUserHelp()
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            if (mKey < 1)
            {
                await chan.SendMessageAsync(NoPerm);
                return;
            }
            else
            {
                var embed = new EmbedBuilder();
                embed.WithColor(Purple);
                embed.WithTitle($"**ADDUSER HELP**");
                embed.WithDescription("Here is the information you requested **" + admin.Username + "**.\n" +
                                            "-admin adduser userName\n **EXAMPLE**: -admin adduser skyyyr\n" +
                                            "-admin adduser userId\n **EXAMPLE**: -admin adduser 123456789\n");
                await chan.SendMessageAsync("", false, embed.Build());
            }
        }
        //Help adding notes to a user account
        [Command("note help")]
        public async Task NoteHelp()
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            if (mKey < 1)
            {
                await chan.SendMessageAsync(NoPerm);
                return;
            }
            else
            {
                var embed = new EmbedBuilder();
                embed.WithColor(Purple);
                embed.WithTitle($"**NOTE HELP**");
                embed.WithDescription("Here is the information you requested " + admin.Username + ".\n" +
                                            "-admin note userName notes\n **EXAMPLE**: -admin note skyyyr Here are a bunch of notes... and even more.\n" +
                                            "-admin note userId notes\n **EXAMPLE**: -admin note 123456789 Here are a bunch of notes... and even more.\n");
                await chan.SendMessageAsync("", false, embed.Build());
            }
        }//Help banning a user from discord
        [Command("ban help")]
        public async Task BanHelp()
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            if (mKey < 1)
            {
                await chan.SendMessageAsync(NoPerm);
                return;
            }
            else
            {
                var embed = new EmbedBuilder();
                embed.WithColor(Purple);
                embed.WithTitle($"**BAN HELP**");
                embed.WithDescription("Here is the information you requested " + admin.Username + ".\n" +
                                            "-admin ban userName notes\n **EXAMPLE**: -admin ban skyyyr because he broke many rules.\n" +
                                            "-admin ban userId notes\n **EXAMPLE**: -admin ban 123456789 because he broke many rules.\n");
                await chan.SendMessageAsync("", false, embed.Build());
            }
        }
        //Help command for kicking a user from discord
        [Command("kick help")]
        public async Task KickHelp()
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            if (mKey < 1)
            {
                await chan.SendMessageAsync(NoPerm);
                return;
            }
            else
            {
                var embed = new EmbedBuilder();
                embed.WithColor(Purple);
                embed.WithTitle($"**KICK HELP**");
                embed.WithDescription("Here is the information you requested " + admin.Username + ".\n" +
                                            "-admin kick userName notes\n **EXAMPLE**: -admin kick skyyyr because he broke many rules.\n" +
                                            "-admin kick userId notes\n **EXAMPLE**: -admin kick 123456789 because he broke many rules.\n");
                await chan.SendMessageAsync("", false, embed.Build());

            }
        }
        //Help command to view admin accounts
        [Command("viewadmin help")]
        public async Task AdminViewAdminHelp()
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            if (mKey < 1)
            {
                await chan.SendMessageAsync(NoPerm);
                return;
            }
            else
            {
                var embed = new EmbedBuilder();
                embed.WithColor(Purple);
                embed.WithTitle($"**VIEWADMIN HELP**");
                embed.WithDescription("Here is the information you requested " + admin.Username + ".\n" +
                                            "-admin viewadmin userName\n **EXAMPLE**: -admin viewadmin skyyyr\n" +
                                            "-admin viewadmin userId\n **EXAMPLE**: -admin viewadmin 123456789\n");
                await chan.SendMessageAsync("", false, embed.Build());
            }
        }//Help command for setting roles on discord
        [Command("setrole help")]
        public async Task AdminSetRoleHelp()
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            if (mKey < 1)
            {
                await chan.SendMessageAsync(NoPerm);
                return;
            }
            else
            {
                var embed = new EmbedBuilder();
                embed.WithColor(Purple);
                embed.WithTitle($"**SETTING USER ROLE HELP**");
                embed.WithDescription("Here is the information you requested " + admin.Username + ".\n" +
                                            "\n" +
                                            "For Role: Active Player type: player\n" +
                                            "For Role: Inactive Player type: inactive\n" +
                                            "For Role: Secured Player type: secured\n" +
                                            "-admin setrole roleId userNameOrUserId notes\n an example: -admin setrole player skyyyr and here are my notes\n");
                await chan.SendMessageAsync("", false, embed.Build());
            }
        }
        //Help command for viewing users in the database
        [Command("viewuser help")]
        public async Task AdminViewUserHelp()
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            if (mKey < 1)
            {
                await chan.SendMessageAsync(NoPerm);
                return;
            }
            else
            {
                var embed = new EmbedBuilder();
                embed.WithColor(Purple);
                embed.WithTitle($"**VIEWUSER HELP**");
                embed.WithDescription("Here is the information you requested " + admin.Username + ".\n" +
                                            "-admin viewuser userName\n **EXAMPLE**: -admin viewuser skyyyr\n" +
                                            "-admin viewuser userId\n **EXAMPLE**: -admin viewuser 123456789\n");
                await chan.SendMessageAsync("", false, embed.Build());
            }
        }
        //Help command for messaging a specific channel
        [Command("message help")]
        public async Task MessageHelp()
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            var dictionary = new Dictionary<string, ulong>();
            dictionary.Add("general", General);
            dictionary.Add("announcements", Announcements);
            dictionary.Add("serverdiscussion", ServerDiscussion);
            dictionary.Add("population", Population);
            dictionary.Add("projectrules", ProjectRules);
            dictionary.Add("fridayfeatures", FridayFeatures);
            dictionary.Add("newmembers", NewMembers);
            dictionary.Add("cantina", Cantina);
            dictionary.Add("screenshots", Screenshots);
            dictionary.Add("topg", TopG);
            dictionary.Add("playerannouncements", PlayerAnnouncements);
            dictionary.Add("playerguides", PlayerGuides);
            dictionary.Add("communitysuggestions", CommunitySuggestions);
            dictionary.Add("cityguildrecruiting", CityGuildRecruiting);
            dictionary.Add("testerarmy", TesterArmy);
            dictionary.Add("lfg", Lfg);
            dictionary.Add("trade", Trade);
            dictionary.Add("medicentertainerbuffers", MedicEntertainerBuffers);
            dictionary.Add("resources", Resources);
            dictionary.Add("crafting", Crafting);
            dictionary.Add("pvp", Pvp);
            dictionary.Add("beastmaster", BeastMaster);
            dictionary.Add("roleplay", Roleplay);
            dictionary.Add("space", Space);
            dictionary.Add("gcw", Gcw);
            dictionary.Add("forumfeed", ForumFeed);
            dictionary.Add("staffannouncements", StaffAnnouncements);
            dictionary.Add("staffcommands", StaffCommands);
            dictionary.Add("staff", Staff);
            dictionary.Add("communitystaff", CommunityStaff);
            dictionary.Add("developmentstaff", DevelopmentStaff);
            dictionary.Add("qastaff", QaDiscussion);
            dictionary.Add("eventdevelopment", EventDevelopment);
            dictionary.Add("stafftraining", StaffTraining);
            dictionary.Add("moderatoractions", ModeratorActions);
            dictionary.Add("consultants", Consultants);
            dictionary.Add("qualityoflife", QualityOfLife);
            dictionary.Add("staffactions", StaffActions);
            dictionary.Add("serverstatus", ServerStatus);
            string listOfChannels = "";
            var task = Task.Run(async () =>
            {
                var list = new List<string>(dictionary.Keys);
                foreach (string key in list) { listOfChannels += "\n" + key; }
                if (mKey < 1)
                {
                    await chan.SendMessageAsync(NoPerm);
                    return;
                }
                else
                {
                    var embed = new EmbedBuilder();
                    embed.WithColor(Purple);
                    embed.WithTitle($"**MESSAGE HELP**");
                    embed.WithDescription("Here is the information you requested " + admin.Username + ".\n" +
                                                "-admin message channelName yourMessage\n **EXAMPLE**: -admin message general Hey guys, lets stop being out of line.\n" +
                                                "**Here's a list of all the channels:\n" + listOfChannels);
                    await chan.SendMessageAsync("", false, embed.Build());
                }
            });
        }
        //Help command for botaccess
        [Command("botaccess help")]
        public async Task BotAccessHelp()
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            if (mKey < 1)
            {
                await chan.SendMessageAsync(NoPerm);
                return;
            }
            else
            {
                var embed = new EmbedBuilder();
                embed.WithColor(Purple);
                embed.WithTitle($"**BOTACCESS HELP**");
                embed.WithDescription("Here is the information you requested " + admin.Username + ".\nThis Command requires level 3 access.\n" +
                                            "-admin botaccess yes userName\n **EXAMPLE**: -admin botaccess 1/2/3 skyyyr\n" +
                                            "-admin botaccess yes userId\n **EXAMPLE**: -admin botaccess 1/2/3 123456789\n" +
                                            "-admin botaccess no userName\n **EXAMPLE**: -admin botaccess 0 skyyyr\n" +
                                            "-admin botaccess no userId\n **EXAMPLE**: -admin botaccess 0 123456789\n");
                await chan.SendMessageAsync("", false, embed.Build());
            }
        }
        //Help command for timeout
        [Command("timeout help")]
        public async Task TimeoutHelp()
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            if (mKey < 1)
            {
                await chan.SendMessageAsync(NoPerm);
                return;
            }
            else
            {
                var embed = new EmbedBuilder();
                embed.WithColor(Purple);
                embed.WithTitle($"**TIMEOUT HELP**");
                embed.WithDescription("Here is the information you requested " + admin.Username + ".\n" +
                                            "-admin timeout username number(1-999) durationType(s=seconds,m=minutes,h=hours) notes\n **EXAMPLE**: -admin timeout skyyyr 1 m  he wouldn't stop being out of line.\n" +
                                            "-admin timeout userId number(1-999) durationType(s=seconds,m=minutes,h=hours)  notes\n **EXAMPLE**: -admin timeout 123456789 1 m he wouldn't stop being out of line.\n");
                await chan.SendMessageAsync("", false, embed.Build());
            }
        }
        [Command("addadmin help")]
        public async Task AdminAddAdminHelp()
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            if (mKey < 1)
            {
                await chan.SendMessageAsync(NoPerm);
                return;
            }
            else
            {
                var embed = new EmbedBuilder();
                embed.WithColor(Purple);
                embed.WithTitle($"**ADDADMIN HELP**");
                embed.WithDescription("Here is the information you requested " + admin.Username + ".\n" +
                                            "-admin addadmin userName\n **EXAMPLE**: -admin addadmin skyyyr\n");
                await chan.SendMessageAsync("", false, embed.Build());
            }
        }
        [Command("gif")]
        public async Task gif(string channel, string type)
        {
            var admin = Context.User;
            var tableName = Database.GetAdminStatus(admin);
            var mKey = tableName.FirstOrDefault().MasterKey;
            var roler = admin as SocketGuildUser;
            var dictionary = new Dictionary<string, ulong>();
            var chan = await Context.Guild.GetChannelAsync(CHANNEL) as SocketTextChannel;
            dictionary.Add("general", General);
            dictionary.Add("announcements", Announcements);
            dictionary.Add("serverdiscussion", ServerDiscussion);
            dictionary.Add("population", Population);
            dictionary.Add("projectrules", ProjectRules);
            dictionary.Add("fridayfeatures", FridayFeatures);
            dictionary.Add("newmembers", NewMembers);
            dictionary.Add("cantina", Cantina);
            dictionary.Add("screenshots", Screenshots);
            dictionary.Add("topg", TopG);
            dictionary.Add("playerannouncements", PlayerAnnouncements);
            dictionary.Add("playerguides", PlayerGuides);
            dictionary.Add("communitysuggestions", CommunitySuggestions);
            dictionary.Add("cityguildrecruiting", CityGuildRecruiting);
            dictionary.Add("testerarmy", TesterArmy);
            dictionary.Add("lfg", Lfg);
            dictionary.Add("trade", Trade);
            dictionary.Add("medicentertainerbuffers", MedicEntertainerBuffers);
            dictionary.Add("resources", Resources);
            dictionary.Add("crafting", Crafting);
            dictionary.Add("pvp", Pvp);
            dictionary.Add("beastmaster", BeastMaster);
            dictionary.Add("roleplay", Roleplay);
            dictionary.Add("space", Space);
            dictionary.Add("gcw", Gcw);
            dictionary.Add("forumfeed", ForumFeed);
            dictionary.Add("staffannouncements", StaffAnnouncements);
            dictionary.Add("staffcommands", StaffCommands);
            dictionary.Add("staff", Staff);
            dictionary.Add("communitystaff", CommunityStaff);
            dictionary.Add("developmentstaff", DevelopmentStaff);
            dictionary.Add("qastaff", QaDiscussion);
            dictionary.Add("eventdevelopment", EventDevelopment);
            dictionary.Add("stafftraining", StaffTraining);
            dictionary.Add("moderatoractions", ModeratorActions);
            dictionary.Add("consultants", Consultants);
            dictionary.Add("qualityoflife", QualityOfLife);
            dictionary.Add("staffactions", StaffActions);
            dictionary.Add("serverstatus", ServerStatus);
            var task = Task.Run(async () =>
            {
                if (mKey < 1)
                {
                    await chan.SendMessageAsync(NoPerm);
                    return;
                }
                else
                {
                    if (dictionary.ContainsKey(channel))
                    {
                        ulong value = dictionary[channel];
                        var test = await Context.Guild.GetChannelAsync(value) as SocketTextChannel;
                        switch (type)
                        {
                            case "merica":
                                await test.SendMessageAsync("https://tenor.com/view/mercia-gif-5966680");
                                break;
                            case "sob":
                                await test.SendMessageAsync("https://tenor.com/view/key-and-peele-son-of-abitch-gif-4276606");
                                break;
                            case "good":
                                await test.SendMessageAsync("https://tenor.com/view/good-evil-starwars-emporerpalpatine-darkside-gif-4624104");
                                break;
                            case "server":
                                await test.SendMessageAsync("https://tenor.com/view/hamster-wheel-gif-8486765");
                                break;
                            case "dude":
                                await test.SendMessageAsync("https://tenor.com/view/comeon-seriously-dude-really-spit-gif-5806216");
                                break;
                            default:
                                break;
                        }
                    }
                }
            });
        }
    }
}
