using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Linq;
using Chester.Configuration;

namespace Chester.Modules.Public_Commands
{
    public class ChatCommands : ModuleBase
    {
        [Command("delay")]
        [Summary("This command will have Maxwell message you after a duration in seconds. \nExample of this: `delay 10 \n\n\n This will delay for 10 seconds")]
        public async Task Delay(int dur, string time, [Remainder] string msg)
        {
            var user = Context.User;
            //var tableName = Database.GetUserStatus(user);
            var task = Task.Run(async () =>
            {
                int newDur = dur;
                dur = dur * 1000;
                int sec = dur;
                int min = dur * 60;
                int hour = dur * 3600;
                string s = "";
                string type = "";
                switch (time)
                {
                    case "hours":
                    case "hour":
                    case "hrs":
                    case "hr":
                        dur = hour;
                        if (hour > (3600 * 1000))
                        {
                            s = "s";
                        }
                        type = " Hour" + s;
                        break;
                    case "minutes":
                    case "minute":
                    case "min":
                    case "m":
                        dur = min;
                        if (min > (60 * 1000))
                        {
                            s = "s";
                        }
                        type = " Minute" + s;
                        break;
                    default:
                        dur = sec;
                        if (sec > 1000)
                        {
                            s = "s";
                        }
                        type = " Second" + s;
                        break;
                }
                await Context.Message.Author.SendMessageAsync("I'll remind you about this: " + msg + " in a duration of " + newDur + type + ".\n");
                await Task.Delay(dur); //1000ms = 1s
                var embed = new EmbedBuilder();
                embed.WithColor(new Color(0x4900ff));
                embed.Title = $"Delayed Task Completed!";
                embed.Description = $" " + msg + "\n";
                await Context.Message.Author.SendMessageAsync("", false, embed);
            });
        }
        private static readonly string[] slotTypes =
        {
            //Rare
            ":sunflower:",
            //Uncommon
            ":star:",
            ":star:",
            //Common
            ":small_blue_diamond:",
            ":rocket:",
            ":poop:",
            ":beetle:",
            ":ghost:",
            //1
            ":small_blue_diamond:",
            ":rocket:",
            ":poop:",
            ":beetle:",
            ":ghost:",
            //2
            ":small_blue_diamond:",
            ":rocket:",
            ":poop:",
            ":beetle:",
            ":ghost:",
            //3
            ":small_blue_diamond:",
            ":rocket:",
            ":poop:",
            ":beetle:",
            ":ghost:",
            //4
            ":small_blue_diamond:",
            ":rocket:",
            ":poop:",
            ":beetle:",
            ":ghost:",
        };
        [Command("Slots")]
        [Alias("slot", "s")]
        [Summary("Slots!")]
        public async Task Slots()
        {
            Random rand = new Random();
            var user = Context.User;
            int randomIndex = rand.Next(slotTypes.Length);
            int randomIndex2 = rand.Next(slotTypes.Length);
            int randomIndex3 = rand.Next(slotTypes.Length);
            int randomIndex4 = rand.Next(slotTypes.Length);
            int randomIndex5 = rand.Next(slotTypes.Length);
            int randomIndex6 = rand.Next(slotTypes.Length);
            int randomIndex7 = rand.Next(slotTypes.Length);
            int randomIndex8 = rand.Next(slotTypes.Length);
            int randomIndex9 = rand.Next(slotTypes.Length);
            int g = 10;
            int b1 = 0;
            int b2 = 0;
            string text = slotTypes[randomIndex];
            string text2 = slotTypes[randomIndex2];
            string text3 = slotTypes[randomIndex3];
            string text4 = slotTypes[randomIndex4];
            string text5 = slotTypes[randomIndex5];
            string text6 = slotTypes[randomIndex6];
            string text7 = slotTypes[randomIndex7];
            string text8 = slotTypes[randomIndex8];
            string text9 = slotTypes[randomIndex9];
            string match = "";
            string fl = "";
            string st = "";
            /*
            if (text == ":sunflower:" && text2 == ":sunflower:" && text3 == ":sunflower:")
            {
                match = "You have matched all the sunflowers! You won the Grand prize of 500 gold!\n";
                g = 500;
                //Database.ChangeGold(user, 500);
            }
            else if (text == text2 && text2 == text3 && text == text3)
            {
                match = "You matched 3 of a kind! You won the prize of 100 gold!\n";
                g = 100;
                //Database.ChangeGold(user, 100);
            }
            else if (text == text2 || text2 == text3 || text == text3)
            {
                match = "You have a pair! You won the prize of 10 gold!\n";
                g = 10;
            }
            else if (text == text2 || text3 == text2 || text2 == text3)
            {
                match = "You have a pair! You won the prize of 10 gold!\n";
                g = 10;
            }
            else
            {
                match = "You didn't get a match worthy of winning and lost 15 gold.\n";
                g = -15;
                //Database.ChangeGold(user, -15);
            }
            if (text == ":star:" || text2 == ":star:" || text3 == ":star:")
            {
                st = "You rolled at least one star and obtained a bonus of 10 gold!\n";
                b1 = b1 + 10;
                //Database.ChangeGold(user, 10);
            }
            if (text == ":sunflower:" || text2 == ":sunflower:" || text3 == ":sunflower:")
            {
                fl = "You rolled at least one sunflower and obtained a bonus of 50 gold!\n";
                b2 = b2 + 50;
                //Database.ChangeGold(user, 50);
            }
                t / t2 / t3
                t4 / t5 / t6
                t7 / t8 / t9
            */
            if (text.equals(text2) && text.equals(text3))
            {
                //left to right 3 of a kind
                match = "You matched 3 of a kind! You won the prize of 100 gold!\n";
            }
            else if (text.equals(text4) && text.equals(text7))
            {
                //up and down 3 of a kind
                match = "You matched 3 of a kind! You won the prize of 100 gold!\n";
            }
            else if (text.equals(text5) && text.equals(text9))
            {
                //diag 3 of kind
                match = "You matched 3 of a kind! You won the prize of 100 gold!\n";
            }
            else if (text2.equals(text5) && text2.equals(text8))
            {
                //up down
                match = "You matched 3 of a kind! You won the prize of 100 gold!\n";
            }
            else if (text3.equals(text6) && text3.equals(text9))
            {
                //up down
                match = "You matched 3 of a kind! You won the prize of 100 gold!\n";
            }
            else if (text3.equals(text5) && text3.equals(text7))
            {
                //diag
                match = "You matched 3 of a kind! You won the prize of 100 gold!\n";
            }
            else if (text4.equals(text5) && text4.equals(text6))
            {
                //left right
                match = "You matched 3 of a kind! You won the prize of 100 gold!\n";
            }
            else if (text7.equals(text8) && text7.equals(text9))
            {
                //left right
                match = "You matched 3 of a kind! You won the prize of 100 gold!\n";
            }
            else
            {
                //no win
                match = "You didn't win you lost 100 gold!\n";
            }
            DateTime now = DateTime.Now;
                var totalAmount = b2 + b1 + g;
                var embed = new EmbedBuilder();
                embed.WithColor(new Color(0x4900ff));
                embed.Title = $"SLOTS!!";
                embed.Description = $"Slot 1: " + text + " | Slot 2: " + text2 + " | Slot 3: " + text3 + "|\n" +
                "Slot 4: " + text4 + " | Slot 5: " + text5 + " | Slot 6: " + text6 + "|\n" +
                "Slot 7: " + text7 + " | Slot 8: " + text8 + " | Slot 9: " + text9 + "|\n" + match + "\n" +
                    st + "\n" +
                    fl + "\n" +
                    "Total Gold: " + totalAmount + "\n" +
                    "Current Gold: ";
                await Context.Message.Author.SendMessageAsync("", false, embed);
            }
        //}
        /*
        [Command("Help")]
        [Alias("help", "h")]
        [Remarks("This command is meant to assist the user with detailed information.")]
        public async Task HelpAsync()
        {
            var embed = new EmbedBuilder();
            embed.WithColor(new Color(0x4900ff));
            embed.Title = $"Player Help Panel";
            embed.Description = $"**Here is a list of all my commands:**\n" +
                                "-daily **(Testing this command)**.\n" +
                                "-I agree\n" +
                                "-8ball yourQuestion\n" +
                                "-delay duration seconds/minutes/hours yourDelayedMessage\n" +
                                "-hello\n" +
                                "-char\n" +
                                "-slots\n" +
                                "-slots help\n" +
                                "-userid\n" +
                                "-execute order66\n";
            await Context.Message.Author.SendMessageAsync("", false, embed);

        }
        Random rand = new Random();
        string[] predictionsTexts = new string[]
        {
            "It is very unlikely.",
            "I don't think so...",
            "Yes!",
            "I don't know",
            "No.",
        };
        [Command("8ball")]
        [Summary("Gives a prediction")]
        public async Task EightBall([Remainder] string input)
        {
            var user = Context.User;
            var tableName = Database.GetUserStatus(user);
            int randomIndex = rand.Next(predictionsTexts.Length);
            string text = predictionsTexts[randomIndex];
            await ReplyAsync("" + text);
        }
        [Command("delay")]
        [Summary("This command will have Maxwell message you after a duration in seconds. \nExample of this: `delay 10 \n\n\n This will delay for 10 seconds")]
        public async Task Delay(int dur, string time, [Remainder] string msg)
        {
            var user = Context.User;
            var tableName = Database.GetUserStatus(user);
            var task = Task.Run(async () =>
            {
                int newDur = dur;
                dur = dur * 1000;
                int sec = dur;
                int min = dur * 60;
                int hour = dur * 3600;
                string s = "";
                string type = "";
                if (time == "hours" || time == "hour" || time == "hrs" || time == "hr")
                {
                    dur = hour;
                    if (hour > (3600 * 1000))
                    {
                        s = "s";
                    }
                    type = " Hour" + s;
                }
                else if (time == "minutes" || time == "minute" || time == "min" || time == "m")
                {
                    dur = min;
                    if (min > (60 * 1000))
                    {
                        s = "s";
                    }
                    type = " Minute" + s;
                }
                else
                {
                    dur = sec;
                    if (sec > 1000)
                    {
                        s = "s";
                    }
                    type = " Second" + s;

                }
                await Context.Message.Author.SendMessageAsync("I'll remind you about this: " + msg + " in a duration of " + newDur + type + ".\n");
                await Task.Delay(dur); //1000ms = 1s
                var embed = new EmbedBuilder();
                embed.WithColor(new Color(0x4900ff));
                embed.Title = $"Delayed Task Completed!";
                embed.Description = $" " + msg + "\n";
                await Context.Message.Author.SendMessageAsync("", false, embed);
            });
        }
        string[] greetingsTexts = new string[]
        {
            "Hello. My name is Maxwell, I'm your butler.",
            "Hi. I'm Maxwell.",
            "Greetings!",
            "Hey.",
            "Well hello there.",
        };
        [Command("hello")]
        [Alias("hi", "hey")]
        [Summary("Gives a greeting.")]
        public async Task Greeting()
        {
            var user = Context.User;
            var tableName = Database.GetUserStatus(user);
            int randomIndex = rand.Next(greetingsTexts.Length);
            string text = greetingsTexts[randomIndex];
            await ReplyAsync("" + text);
        }
        string[] slotTypes = new string[]
        {
            //Rare
            ":sunflower:",
            //Uncommon
            ":star:",
            ":star:",
            //Common
            ":small_blue_diamond:",
            ":rocket:",
            ":poop:",
            ":beetle:",
            ":ghost:",
            //1
            ":small_blue_diamond:",
            ":rocket:",
            ":poop:",
            ":beetle:",
            ":ghost:",
            //2
            ":small_blue_diamond:",
            ":rocket:",
            ":poop:",
            ":beetle:",
            ":ghost:",
            //3
            ":small_blue_diamond:",
            ":rocket:",
            ":poop:",
            ":beetle:",
            ":ghost:",
            //4
            ":small_blue_diamond:",
            ":rocket:",
            ":poop:",
            ":beetle:",
            ":ghost:",
        };
        [Command("Slots Help")]
        public async Task SlotsHelp()
        {
            var user = Context.User;
            var tableName = Database.GetUserStatus(user);
            await Context.Message.Author.SendMessageAsync(Context.User.Mention + " Here is some information about slots.\n" +
            "There are a total of 7 different icons that are being rolled\n" +
            "**COMMON ICONS**\n" +
            "\t:small_blue_diamond:\n" +
            "\t:rocket:\n" +
            "\t:poop:\n" +
            "\t:beetle:\n" +
            "\t:ghost:\n" +
            "**UNCOMMON & RARE**\n" +
            "\t:star:\n" +
            "\t:sunflower:\n" +
            "**REWARDS AND STARTING INFO**\n" +
            "\tYou start out with 100 gold, and if you drop below 15 gold you will be automatically granted 100 more.\n" +
            "\tGetting at least 1 sunflower grants an additional 50 gold.\n" +
            "\tGetting at least 1 star grants an additional 10 gold.\n" +
            "\tGetting all 3 sunflowers grants you 500 gold, and that addition of 50 gold.\n" +
            "\tGetting all 3 slots the same grants you 100 gold, along with any other bonus you may obtain.\n" +
            "\tGetting at least 2 icons the same grants you 10 gold, along with any other bonus you may obtain. \n" +
            "\tNot getting any matches will grant you -15 gold, you may still get rewarded for any stars, or sunflowers you happen to roll.\n");
        }
        /*
        [Command("Slots")]
        [Alias("slot", "s")]
        [Summary("Slots!")]
        public async Task Slots()
        {
            var user = Context.User;
            var tableName = Database.GetUserStatus(user);
            var agree = tableName.FirstOrDefault().Agree;
            if (agree < 1) { await Context.Message.Author.SendMessageAsync("I'm sorry but you must -I agree before issuing this command."); return; }
            var goldValue = tableName.FirstOrDefault().Gold;
            if (goldValue <= 14)
            {
                await Context.Channel.SendMessageAsync("You ran out of money! Be sure to claim your daily reward gold by using: -daily\n\n");
            }
            int randomIndex = rand.Next(slotTypes.Length);
            int randomIndex2 = rand.Next(slotTypes.Length);
            int randomIndex3 = rand.Next(slotTypes.Length);
            int g = 10;
            int b1 = 0;
            int b2 = 0;
            string text = slotTypes[randomIndex];
            string text2 = slotTypes[randomIndex2];
            string text3 = slotTypes[randomIndex3];
            string match = "";
            string fl = "";
            string st = "";
            if (text == ":sunflower:" && text2 == ":sunflower:" && text3 == ":sunflower:")
            {
                match = "You have matched all the sunflowers! You won the Grand prize of 500 gold!\n";
                g = 500;
                //Database.ChangeGold(user, 500);
            }
            else if (text == text2 && text2 == text3 && text == text3)
            {
                match = "You matched 3 of a kind! You won the prize of 100 gold!\n";
                g = 100;
                //Database.ChangeGold(user, 100);
            }
            else if (text == text2 || text2 == text3 || text == text3)
            {
                match = "You have a pair! You won the prize of 10 gold!\n";
                g = 10;
            }
            else
            {
                match = "You didn't get a match worthy of winning and lost 15 gold.\n";
                g = -15;
                //Database.ChangeGold(user, -15);
            }
            if (text == ":star:" || text2 == ":star:" || text3 == ":star:")
            {
                st = "You rolled at least one star and obtained a bonus of 10 gold!\n";
                b1 = b1 + 10;
                //Database.ChangeGold(user, 10);
            }
            if (text == ":sunflower:" || text2 == ":sunflower:" || text3 == ":sunflower:")
            {
                fl = "You rolled at least one sunflower and obtained a bonus of 50 gold!\n";
                b2 = b2 + 50;
                //Database.ChangeGold(user, 50);
            }
            DateTime now = DateTime.Now;
            DateTime last = tableName.FirstOrDefault().Last;
            if (last.Minute < now.Minute)
            {
                var totalAmount = b2 + b1 + g;
                var newG = goldValue + totalAmount;
                Database.ChangeGold(user, totalAmount);
                var embed = new EmbedBuilder();
                embed.WithColor(new Color(0x4900ff));
                embed.Title = $"SLOTS!!";
                embed.Description = $"Slot 1: " + text + " | Slot 2: " + text2 + " | Slot 3: " + text3 + "|\n" + match + "\n" +
                    st + "\n" +
                    fl + "\n" +
                    "Total Gold: " + totalAmount + "\n" +
                    "Current Gold: " + newG;
                await Context.Message.Author.SendMessageAsync("", false, embed);
            }
            else
            {
                await Context.Message.Author.SendMessageAsync("You're still on cooldown. (1 minute)");
            }
        }
        /*
        [Command("embed")]
        [Summary("Puts your message in an embed format.")]
        public async Task Embder([Remainder] string msg)
        {
            var user = Context.User;
            var tableName = Database.GetUserStatus(user);
            var agree = tableName.FirstOrDefault().Agree;
            if (agree < 1) { return; }
            var embed = new EmbedBuilder();
            embed.WithColor(new Color(0x4900ff));
            embed.Title = $"Custom Message!";
            embed.Description = $"\n" + msg;
            await Context.Message.Author.SendMessageAsync(Context.User.Mention + "", false, embed);
            //await Context.Channel.SendMessageAsync("", false, embed);
        }
        
        [Command("userId")]
        [Summary("Maxwell will send you your userId.")]
        public async Task UserId()
        {
            var user = Context.User;
            var tableName = Database.GetUserStatus(user);
            var embed = new EmbedBuilder();
            embed.WithColor(new Color(0x4900ff));
            embed.Title = $"Your user id";
            embed.Description = $"user id: " + Context.User.Id + "\n";
            await Context.Message.Author.SendMessageAsync("", false, embed);
        }
        [Command("execute order66")]
        public async Task Order66()
        {
            var user = Context.User;
            var tableName = Database.GetUserStatus(user);
            await ReplyAsync("Yes my lord... Wait I'm not a clone trooper!");
        }
        /*
        [Command("daily")]
        public async Task Daily()
        {
            var user = Context.User;
            var tableName = Database.GetUserStatus(user);
            var agree = tableName.FirstOrDefault().Agree;
            if (agree < 1) { await Context.Message.Author.SendMessageAsync("I'm sorry but you must -I agree before issuing this command."); return; }
            DateTime now = DateTime.Now;
            DateTime daily = tableName.FirstOrDefault().Daily;
            int difference = DateTime.Compare(daily, now);
            if ((tableName.FirstOrDefault().Daily.ToString() == "0001-01-01 00:00:00") || (daily.Day < now.Day)) //&& difference < 0 || difference >= 0))
            {
                Database.ChangeDaily(user);
                int amount = 250;
                Database.ChangeGold(user, amount);
                await Context.Message.Author.SendMessageAsync($"**[TESTING]**(Can be used once per day **not a 24 hour cooldown**) You received your daily amount of {amount} gold!");
            }
            else
            {
                TimeSpan diff = now - daily;
                //TimeSpan di = new TimeSpan(23 - diff.Hours, 60 - diff.Minutes, 60 - diff.Seconds);
                //TimeSpan di = new TimeSpan(1 - diff.days);
                await Context.Message.Author.SendMessageAsync($"**[TESTING]** You've used your daily command today already. Try again tomorrow.");
            }
        }
        
        [Command("char")]
        [Alias("c")]
        public async Task Character()
        {
            var user = Context.User;
            var tableName = Database.GetUserStatus(user);
            var embed = new EmbedBuilder();
            embed.WithColor(new Color(0x4900ff));
            embed.Title = $"**" + user + "Character Sheet";
            embed.Description = user.Username + "'s current stats is as follows: \n" +
                ":black_small_square: User ID: " + tableName.FirstOrDefault().UserId + ".\n";
            await Context.Message.Author.SendMessageAsync("", false, embed);
        }

        /*
        [Command("m")]
        [Summary("")]
        public async Task E()
        {
            string text = WhiteList.Load().User_id;
            await ReplyAsync("ID:" + text);
        }
        */
    }
}
