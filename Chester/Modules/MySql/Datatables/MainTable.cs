using System;
using System.Collections.Generic;
using System.Text;

namespace Chester
{
    public class MainTable
    {
        //This is the main table, ONLY  essential information about the discord user account she be in here.
        //Any information regarding user data, should go to the UserdataTable.cs
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string ForumName { get; set; }
        public string WhoAdded { get; set; }
        public DateTime JoinDate { get; set; }
        public string WhoRoled { get; set; }
    }
}