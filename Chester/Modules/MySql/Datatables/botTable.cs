using System;
using System.Collections.Generic;
using System.Text;

namespace Chester.Modules.MySql.Datatables
{
    public class BotTable
    {
    /*
         ADJUST THIS TO BE MAXWELL USER ID AND MAKE NEW DATABASE FUNCTION CALLS TO ALWAYS CALL HIS ID TO GRAB THIS INFO
    */
        public string UserJoinedMessage { get; set; }//Message displayed when a user joins the server
        public string PrintAdminCommand { get; set; }//Will the bot send messages to specific channel whenever an admin uses a cmd (cmd logs)
        public string AdminLevelToggle { get; set; }//Sets the level requirement checks on or off

        public string SetLevelNote { get; set; }//Sets the level required to put notes
        public string SetLevelKick { get; set; }//Sets the level required to kick users
        public string SetLevelBan { get; set; }//Sets the level required to ban users
        public string SetLevelRole { get; set; }//Sets the level required to manage roles
        public string SetLevelAdd { get; set; }//Sets the level required to add users to db
        public string SetLevelAddAdmin { get; set; }//Sets the level required to add admins to db
        public string SetLevelModify { get; set; }//Sets the level required to modify this datatable

        public string DebugMessageChannel { get; set; }//This is the channel that the bot will send debug messages to
        public string CommandMessageChannel { get; set; }//This is the channel all cmd messages will get sent(Cmd log within a channel on discord)

        public string ToggleDebugMessage { get; set; }//Toggles the debug messaging
        public string ToggleCommandMessage { get; set; }//Toggles the cmd messaging
    }
}