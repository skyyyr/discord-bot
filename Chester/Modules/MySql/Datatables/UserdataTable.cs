using System;
using System.Collections.Generic;
using System.Text;

namespace Chester.Modules.MySql.Datatables
{
    public class UserdataTable
    {
        public string UserName { get; set; }
        public string UserId { get; set; }
        public string UserRole { get; set; }
        
        //When user was ..
        public DateTime WhenPromoted { get; set; }
        public DateTime WhenTimeout { get; set; }
        public DateTime WhenKick { get; set; }
        public DateTime WhenBan { get; set; }

        public string TimesNote { get; set; }//Times user has noted account
        public string TimesKick { get; set; }//Times user has kicked user
        public string TimesBan { get; set; }//Times user banned user
        public string TimesTimeout { get; set; }//Times user timeout
        public string TimesJoined { get; set; }//Times user joined discord server.
        //Admin user who did certain actions to user
        public string WhoNoteMe { get; set; }
        public string WhoKickMe { get; set; }
        public string WhoBanMe { get; set; }
        public string WhoTimeoutMe { get; set; }
        public string WhoRoleMe { get; set; }
    }
}