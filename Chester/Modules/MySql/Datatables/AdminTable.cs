using System;
using System.Collections.Generic;
using System.Text;

namespace Chester.Modules.MySql.Datatables
{
    public class AdminTable
    {
        public string UserId { get; set; }//Admin user's ID
        public string AdminName { get; set; }//Admin user's name
        public int MasterKey { get; set; }//Gives master key to admin, essentially full access.
        public int AdminLevel { get; set; }//Admin level for commands

        public DateTime LastNote { get; set; }//Last time admin left a note on an account
        public DateTime LastKick { get; set; }//Last time admin kicked a player
        public DateTime LastBan { get; set; }//Last time admin banned a player
        public DateTime LastRole { get; set; }//Last time admin changed a player role
        public DateTime LastAdd { get; set; }//Last time admin added a user to the db
        public DateTime LastAddAdmin { get; set; }//Last time admin added an admin to the admin db
        public DateTime LastModify { get; set; }//Last time admin modified the bot db
        public DateTime WhenAdded { get; set; }//When the admin was added to the admin tables
    }
}