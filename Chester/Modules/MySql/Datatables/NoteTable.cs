using System;
using System.Collections.Generic;
using System.Text;

namespace Chester.Modules.MySql.Datatables
{
    public class NoteTable
    {
        public string UserName { get; set; }
        public string UserId { get; set; }
        public string GeneralNotes { get; set; }
        public string KickNotes { get; set; }
        public string BanNotes { get; set; }
        public string DemoteNotes { get; set; }
        public string PromoteNotes { get; set; }
        public string TimeoutNotes { get; set; }
    }
}