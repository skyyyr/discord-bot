using Discord;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using Chester.Modules.MySql.Datatables;

namespace Chester
{
    public class Database
    {
        private string table { get; set; }
        private const string server = "localhost";
        private const string database = "database";
        private const string username = "root";
        private const string password = "12345";
        private MySqlConnection dbConnection;
        public Database(string table)
        {
            this.table = table;
            MySqlConnectionStringBuilder stringBuilder = new MySqlConnectionStringBuilder();
            stringBuilder.Server = server;
            stringBuilder.UserID = username;
            stringBuilder.Password = password;
            stringBuilder.Database = database;
            stringBuilder.SslMode = MySqlSslMode.None;
            var connectionString = stringBuilder.ToString();
            dbConnection = new MySqlConnection(connectionString);
            dbConnection.Open();
        }
        public MySqlDataReader FireCommand(string query)
        {
            if (dbConnection == null)
            {
                return null;
            }
            MySqlCommand command = new MySqlCommand(query, dbConnection);
            var mySqlReader = command.ExecuteReader();
            return mySqlReader;
        }
        public void CloseConnection()
        {
            if (dbConnection != null)
            {
                dbConnection.Close();
            }
        }
        /*
            CHECK FOR USER IN DATABASE        
        */
        //Check main table for existing user
        public static List<String> CheckExistingUser(IUser user)
        {
            var result = new List<String>();
            var database = new Database("database");
            var str = string.Format("SELECT * FROM maintable WHERE userId = '{0}'", user.Id);
            var tableName = database.FireCommand(str);
            while (tableName.Read())
            {
                var userId = (string)tableName["userId"];
                result.Add(userId);
            }
            database.CloseConnection();
            return result;
        }
        //Check admin table for existing user
        public static List<String> CheckExistingAdmin(IUser user)
        {
            var result = new List<String>();
            var database = new Database("database");
            var str = string.Format("SELECT * FROM admintable WHERE userId = '{0}'", user.Id);
            var tableName = database.FireCommand(str);
            while (tableName.Read())
            {
                var userId = (string)tableName["userId"];
                result.Add(userId);
            }
            database.CloseConnection();
            return result;
        }
        //Check userdata table for existing user
        public static List<String> CheckExistingUserdata(IUser user)
        {
            var result = new List<String>();
            var database = new Database("database");
            var str = string.Format("SELECT * FROM userdatatable WHERE userId = '{0}'", user.Id);
            var tableName = database.FireCommand(str);
            while (tableName.Read())
            {
                var userId = (string)tableName["userId"];
                result.Add(userId);
            }
            database.CloseConnection();
            return result;
        }
        //Check note table for existing user
        public static List<String> CheckExistingNote(IUser user)
        {
            var result = new List<String>();
            var database = new Database("database");
            var str = string.Format("SELECT * FROM notetable WHERE userId = '{0}'", user.Id);
            var tableName = database.FireCommand(str);
            while (tableName.Read())
            {
                var userId = (string)tableName["userId"];
                result.Add(userId);
            }
            database.CloseConnection();
            return result;
        }
        /*
            ADD USERS TO DATABASE
        */
        //adds user to main table
        public static string EnterUserMainTable(IUser user)
        {
            var database = new Database("database");
            var str = string.Format("INSERT INTO maintable (userId, userName, joinDate, forumName) VALUES ('{0}', '{1}', curtime(), 'unknown')", user.Id, user.Username);
            var table = database.FireCommand(str);
            database.CloseConnection();
            return null;
        }
        //adds user to note table
        public static string EnterUserNoteTable(IUser user)
        {
            var database = new Database("database");
            var str = string.Format("INSERT INTO notetable (userId, userName, generalNotes, kickNotes, banNotes, promoteNotes, timeoutNotes) VALUES ('{0}', '{1}', 'none', 'none', 'none', 'none', 'none')", user.Id, user.Username);
            var table = database.FireCommand(str);
            database.CloseConnection();
            return null;
        }
        //adds user to userdata table
        public static string EnterUserUserdataTable(IUser user)
        {
            var database = new Database("database");
            var str = string.Format("INSERT INTO userdatatable (userId, userName, whenKick, whenBan, whenPromoted, whenTimeout) VALUES ('{0}', '{1}', curtime(), curtime(), curtime(), curtime())", user.Id, user.Username);
            var table = database.FireCommand(str);
            database.CloseConnection();
            return null;
        }
        //adds user to admin table
        public static string EnterUserAdminTable(IUser user)
        {
            var database = new Database("database");
            var str = string.Format("INSERT INTO admintable (userId, adminName, adminLevel, masterKey, lastAdd, whenAdded, lastRole, lastAddAdmin, lastNote, lastKick, lastBan) VALUES ('{0}', '{1}', '1', '0', curtime(), curtime(), curtime(), curtime(), curtime(), curtime(), curtime())", user.Id, user.Username);
            var table = database.FireCommand(str);
            database.CloseConnection();
            return null;
        }
        //remove admin user
        public static string RemoveUserAdminTable(IUser user)
        {
            var database = new Database("database");
            var str = string.Format("DELETE FROM admintable WHERE adminName = '{0}'", user.Username);
            var table = database.FireCommand(str);
            database.CloseConnection();
            return null;
        }
        /*
            DISPLAY DATATABLE HELPERS
        */
        //AdminTable getAdminStatus
        public static List<AdminTable> GetAdminStatus(IUser user)
        {
            var result = new List<AdminTable>();
            var database = new Database("database");
            var str = string.Format("SELECT * FROM admintable WHERE userId = '{0}'", user.Id);
            var tableName = database.FireCommand(str);
            while (tableName.Read())
            {
                var userId = (string)tableName["userId"];
                var userName = (string)tableName["adminName"];
                var masterKey = (int)tableName["masterKey"];
                var adminLevel = (int)tableName["adminLevel"];
                result.Add(new AdminTable
                {
                    UserId = userId,
                    AdminName = userName,
                    AdminLevel = adminLevel,
                    MasterKey = masterKey
                });
            }
            database.CloseConnection();
            return result;
        }
        //AdminTable getAdminWhenStatus
        public static List<AdminTable> GetAdminWhenStatus(IUser user)
        {
            var result = new List<AdminTable>();
            var database = new Database("database");
            var str = string.Format("SELECT * FROM admintable WHERE userId = '{0}'", user.Id);
            var tableName = database.FireCommand(str);
            while (tableName.Read())
            {
                var userId = (string)tableName["userId"];
                /*
                var lastNote = (DateTime)tableName["lastNote"];
                var lastModify = (DateTime)tableName["lastModify"];
                */
                var lastAdd = (DateTime)tableName["lastAdd"];
                var lastRole = (DateTime)tableName["lastRole"];
                var lastAddAdmin = (DateTime)tableName["lastAddAdmin"];
                var whenAdded = (DateTime)tableName["whenAdded"];
                var lastKick = (DateTime)tableName["lastKick"];
                var lastBan = (DateTime)tableName["lastBan"];
                var lastNote = (DateTime)tableName["lastNote"];
                result.Add(new AdminTable
                {
                    UserId = userId,
                    LastAddAdmin = lastAddAdmin,
                    LastAdd = lastAdd,
                    LastRole = lastRole,
                    LastKick = lastKick,
                    LastBan = lastBan,
                    LastNote = lastNote,
                    WhenAdded = whenAdded
                    /*
                    
                    LastKick = lastKick,
                    LastModify = lastModify,
                    LastNote = lastNote,
                    */
                });
            }
            database.CloseConnection();
            return result;
        }
        //MainTable getMainStatus
        public static List<MainTable> GetMainStatus(IUser user)
        {
            var result = new List<MainTable>();
            var database = new Database("database");
            var str = string.Format("SELECT * FROM maintable WHERE userId = '{0}'", user.Id);
            var tableName = database.FireCommand(str);
            while (tableName.Read())
            {
                var userId = (string)tableName["userId"];
                var userName = (string)tableName["userName"];
                var joinDate = (DateTime)tableName["joinDate"];
                var forumName = (string)tableName["forumName"];
                var whoAdded = (string)tableName["whoAdded"];
                var whoRoled = (string)tableName["whoRoled"];
                result.Add(new MainTable
                {
                    UserId = userId,
                    UserName = userName,
                    JoinDate = joinDate,
                    ForumName = forumName,
                    WhoAdded = whoAdded,
                    WhoRoled = whoRoled
                });
            }
            database.CloseConnection();
            return result;
        }
        //NoteTable getNoteStatus
        public static List<NoteTable> GetNoteStatus(IUser user)
        {
            var result = new List<NoteTable>();
            var database = new Database("database");
            var str = string.Format("SELECT * FROM notetable WHERE userId = '{0}'", user.Id);
            var tableName = database.FireCommand(str);
            while (tableName.Read())
            {
                var userId = (string)tableName["userId"];
                var userName = (string)tableName["userName"];
                var generalNotes = (string)tableName["generalNotes"];
                var banNotes = (string)tableName["banNotes"];
                var kickNotes = (string)tableName["kickNotes"];
                var promoteNotes = (string)tableName["promoteNotes"];
                var timeoutNotes = (string)tableName["timeoutNotes"];
                result.Add(new NoteTable
                {
                    UserId = userId,
                    UserName = userName,
                    GeneralNotes = generalNotes,
                    BanNotes = banNotes,
                    KickNotes = kickNotes,
                    PromoteNotes = promoteNotes,
                    TimeoutNotes = timeoutNotes

                });
            }
            database.CloseConnection();
            return result;
        }
        //UserdataTable getUserdataStatus
        public static List<UserdataTable> GetUserdataStatus(IUser user)
        {
            var result = new List<UserdataTable>();
            var database = new Database("database");
            var str = string.Format("SELECT * FROM userdatatable WHERE userId = '{0}'", user.Id);
            var tableName = database.FireCommand(str);
            while (tableName.Read())
            {
                var userId = (string)tableName["userId"];
                var userName = (string)tableName["userName"];
                var userRole = (string)tableName["userRole"];
                var whenKick = (DateTime)tableName["whenKick"];
                var whenBan = (DateTime)tableName["whenBan"];
                var whenPromoted = (DateTime)tableName["whenPromoted"];
                var whenTimeout = (DateTime)tableName["whenTimeout"];
                var whoNoteMe = (string)tableName["whoNoteMe"];
                var whoRoleMe = (string)tableName["whoRoleMe"];
                var whoTimeoutMe = (string)tableName["whoTimeoutMe"];
                var whoKickMe = (string)tableName["whoKickMe"];
                var whoBanMe = (string)tableName["whoBanMe"];
                result.Add(new UserdataTable
                {
                    UserId = userId,
                    UserName = userName,
                    UserRole = userRole,
                    WhenKick = whenKick,
                    WhenBan = whenBan,
                    WhenPromoted = whenPromoted,
                    WhenTimeout = whenTimeout,
                    WhoNoteMe = whoNoteMe,
                    WhoRoleMe = whoRoleMe,
                    WhoTimeoutMe = whoTimeoutMe,
                    WhoBanMe = whoBanMe,
                    WhoKickMe = whoKickMe
                });
            }
            database.CloseConnection();
            return result;
        }
        //Get admincommandlevel
        //Get user notehistory
        /*
            CHANGE DATA TABLE ENTRY
        */

        //Admin Tables
        //Change Admin level
        public static void ChangeAdminLevel(IUser user, string lvl)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE admintable SET lvl = '{1}' WHERE userId = {0}", user.Id, lvl);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Change master key
        public static void ChangeMasterKey(IUser user, int key)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE admintable SET masterKey = '{1}' WHERE userId = {0}", user.Id, key);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Who added who
        public static void WhoAddedWho(IUser user, IUser admin)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE maintable SET whoAdded = '{1}' WHERE userId = {0}", user.Id, admin.Username);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Who note me
        public static void WhoNoteMe(IUser user, IUser admin)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE userdatatable SET whoNoteMe = '{1}' WHERE userId = {0}", user.Id, admin.Username);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Who timeout me
        public static void WhoTimeoutMe(IUser user, IUser admin)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE userdatatable SET whoTimeoutMe = '{1}' WHERE userId = {0}", user.Id, admin.Username);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Who ban me
        public static void WhoBanMe(IUser user, IUser admin)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE userdatatable SET whoBanMe = '{1}' WHERE userId = {0}", user.Id, admin.Username);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Who kick me
        public static void WhoKickMe(IUser user, IUser admin)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE userdatatable SET whoKickMe = '{1}' WHERE userId = {0}", user.Id, admin.Username);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Who role me
        public static void WhoRoleMe(IUser user, IUser admin)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE userdatatable SET whoRoleMe = '{1}' WHERE userId = {0}", user.Id, admin.Username);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Update last note
        public static void ChangeLastNote(IUser user)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format($"UPDATE admintable SET lastNote = curtime() WHERE userId = '{user.Id}'");
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
            }
            catch (Exception)
            {
                database.CloseConnection();
            }
        }
        //Update last kick
        public static void ChangeLastKick(IUser user)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format($"UPDATE admintable SET lastKick = curtime() WHERE userId = '{user.Id}'");
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
            }
            catch (Exception)
            {
                database.CloseConnection();
            }
        }
        //Update last ban
        public static void ChangeLastBan(IUser user)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format($"UPDATE admintable SET lastBan = curtime() WHERE userId = '{user.Id}'");
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
            }
            catch (Exception)
            {
                database.CloseConnection();
            }
        }
        //Update last role
        public static void ChangeLastRole(IUser user)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format($"UPDATE admintable SET lastRole = curtime() WHERE userId = '{user.Id}'");
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
            }
            catch (Exception)
            {
                database.CloseConnection();
            }
        }
        //Update last add
        public static void ChangeLastAdd(IUser user)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format($"UPDATE admintable SET lastAdd = curtime() WHERE userId = '{user.Id}'");
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
            }
            catch (Exception)
            {
                database.CloseConnection();
            }
        }
        //Update last add admin
        public static void ChangeLastAddAdmin(IUser user)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format($"UPDATE admintable SET lastAddAdmin = curtime() WHERE userId = '{user.Id}'");
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
            }
            catch (Exception)
            {
                database.CloseConnection();
            }
        }
        //Update last modify
        //ADD FUNCTION
        public static void ChangeLastModify(IUser user)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format($"UPDATE admintable SET lastModify = curtime() WHERE userId = '{user.Id}'");
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
            }
            catch (Exception)
            {
                database.CloseConnection();
            }
        }
        //START ADD COMMANDS
        //Change can note
        public static void ChangeCanNote(IUser user, int can)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE admintable SET canNote = '{1}' WHERE userId = {0}", user.Id, can);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Change can kick
        public static void ChangeCanKick(IUser user, int can)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE admintable SET canKick = '{1}' WHERE userId = {0}", user.Id, can);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Change can ban
        public static void ChangeCanBan(IUser user, int can)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE admintable SET canBan = '{1}' WHERE userId = {0}", user.Id, can);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Change can role
        public static void ChangeCanRole(IUser user, int can)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE admintable SET canRole = '{1}' WHERE userId = {0}", user.Id, can);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Change can add
        public static void ChangeCanAdd(IUser user, int can)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE admintable SET canAdd = '{1}' WHERE userId = {0}", user.Id, can);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Change can add admin
        public static void ChangeCanAddAdmin(IUser user, int can)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE admintable SET canAddAdmin = '{1}' WHERE userId = {0}", user.Id, can);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Change can modify
        public static void ChangeCanModify(IUser user, int can)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE admintable SET canModify = '{1}' WHERE userId = {0}", user.Id, can);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //END NEW COMMAND
        //Main table
        public static void ChangeForumName(IUser user, string name)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE maintable SET forumName = '{1}' WHERE userId = {0}", user.Id, name);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Note Table
        //Update note general
        public static void UpdateNoteGeneral(IUser user, string note)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE notetable SET generalNotes = '{1}' WHERE userId = {0}", user.Id, note);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Update note kick
        public static void UpdateNoteKick(IUser user, string note)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE notetable SET kickNotes = '{1}' WHERE userId = {0}", user.Id, note);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Update note ban
        public static void UpdateNoteBan(IUser user, string note)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE notetable SET banNotes = '{1}' WHERE userId = {0}", user.Id, note);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Update note promote
        public static void UpdateNotePromote(IUser user, string note)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE notetable SET promoteNotes = '{1}' WHERE userId = {0}", user.Id, note);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //Userdata
        //Change user role
        public static void ChangeUserRole(IUser user, string role)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format("UPDATE userdatatable SET userRole = '{1}' WHERE userId = {0}", user.Id, role);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            }
            catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
        //When promoted
        public static void WhenPromoted(IUser user)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format($"UPDATE userdatatable SET whenPromoted = curtime() WHERE userId = '{user.Id}'");
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
            }
            catch (Exception)
            {
                database.CloseConnection();
            }
        }
        //When timeout
        public static void WhenTimeout(IUser user)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format($"UPDATE userdatatable SET whenTimeout = curtime() WHERE userId = '{user.Id}'");
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
            }
            catch (Exception)
            {
                database.CloseConnection();
            }
        }
        //When kick
        public static void WhenKick(IUser user)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format($"UPDATE userdatatable SET whenKick = curtime() WHERE userId = '{user.Id}'");
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
            }
            catch (Exception)
            {
                database.CloseConnection();
            }
        }
        //When ban
        public static void WhenBan(IUser user)
        {
            var database = new Database("database");
            try
            {
                var strings = string.Format($"UPDATE userdatatable SET whenBan = curtime() WHERE userId = '{user.Id}'");
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
            }
            catch (Exception)
            {
                database.CloseConnection();
            }
        }
    }
}