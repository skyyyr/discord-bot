﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace Chester.Configuration
{
    public class BotConfig
    {
        [JsonIgnore]
        public static readonly string appdir = AppContext.BaseDirectory;

        private string Prefix;
        private string Token;

        public BotConfig()
        {

        }

        public void SetToken(string Token)
        {
            this.Token = Token;
        }

        public void SetPrefix(string Prefix)
        {
            this.Prefix = Prefix;
        }

        public string GetToken()
        {
            return Token;
        }

        public string GetPrefix()
        {
            return Prefix;
        }

        public void Save(string dir = "configuration/config.json")
        {
            string file = Path.Combine(appdir, dir);
            File.WriteAllText(file, ToJson());
        }

        public static BotConfig Load(string dir = "configuration/config.json")
        {
            string file = Path.Combine(appdir, dir);
            return JsonConvert.DeserializeObject<BotConfig>(File.ReadAllText(file));
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}