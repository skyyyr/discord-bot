using System;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Discord.Commands;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using Chester.Configuration;

namespace Chester
{
    /*
     * TODO LIST:
     * - Command to update channel for output
     * - Admin command levels commands
     * - Note history commands
     * - Database.cs ->
     *       - CheckNoteHistory
     *       - CheckCommandLevel
     *       - ChangeAdminLevel -> not being used
     *       - ChangeForumName -> not being used
     */
    public class Program
    {
        public static void Main(string[] args) =>
            new Program().Start().GetAwaiter().GetResult();

        private DiscordSocketClient client;
        private CommandHandler handler;

        public async Task Start()
        {
            EnsureBotConfigExists();
            client = new DiscordSocketClient(new DiscordSocketConfig()
            {
                LogLevel = LogSeverity.Verbose,
            });
            client.Log += Logger;
            await client.LoginAsync(TokenType.Bot, BotConfig.Load().Token);
            await client.StartAsync();
            var serviceProvider = ConfigureServices();
            handler = new CommandHandler(serviceProvider);
            await handler.ConfigureAsync();
            await Task.Delay(-1);
        }
        private static Task Logger(LogMessage lmsg)
        {
            var cc = Console.ForegroundColor;
            switch (lmsg.Severity)
            {
                case LogSeverity.Critical:
                case LogSeverity.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case LogSeverity.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case LogSeverity.Info:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case LogSeverity.Verbose:
                case LogSeverity.Debug:
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    break;
            }
            Console.WriteLine($"{DateTime.Now} [{lmsg.Severity,8}] {lmsg.Source}: {lmsg.Message}");
            Console.ForegroundColor = cc;
            return Task.CompletedTask;
        }

        public static void EnsureBotConfigExists()
        {
            if (!Directory.Exists(Path.Combine(AppContext.BaseDirectory, "configuration")))
                Directory.CreateDirectory(Path.Combine(AppContext.BaseDirectory, "configuration"));
            string loc = Path.Combine(AppContext.BaseDirectory, "configuration/config.json");
            if (!File.Exists(loc))
            {
                var config = new BotConfig();
                Console.WriteLine("Please enter the following information to save into your configuration/config.json file");
                Console.Write("Bot Token: ");
                config.Token = Console.ReadLine();
                Console.Write("Bot Prefix: ");
                config.Prefix = Console.ReadLine();
                config.Save();
            }
            Console.WriteLine($"{ DateTime.Now} [CONFIG] Bot configuration file has been loaded.");
        }
        public IServiceProvider ConfigureServices()
        {
            var services = new ServiceCollection()
            .AddSingleton(client)
            .AddSingleton(new CommandService(new CommandServiceConfig { CaseSensitiveCommands = false }));
            var provider = new DefaultServiceProviderFactory().CreateServiceProvider(services);
            return provider;
        }
    }
}