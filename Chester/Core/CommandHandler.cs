using System.Threading.Tasks;
using System.Reflection;
using Discord.Commands;
using Discord.WebSocket;
using System;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using Discord;
using Chester.Configuration;
using System.Text.RegularExpressions;

namespace Chester
{
    public class CommandHandler
    {
        private CommandService commands;
        private DiscordSocketClient bot;
        private IServiceProvider map;
        public DateTime Timestamp { get; set; }

        public CommandHandler(IServiceProvider provider)
        {
            //These are how we handle our methods, and what triggers them.
            map = provider;
            bot = map.GetService<DiscordSocketClient>();
            bot.UserJoined += AnnounceUserJoined;
            bot.MessageReceived += HandleCommand;
            bot.MessageReceived += HandleBadWords;
            bot.MessageReceived += TalkToMe;
            bot.UserLeft += HandleAdminLeave;
            commands = map.GetService<CommandService>();
        }
        public async Task AnnounceUserJoined(SocketGuildUser user)
        {
            //This method will alert the console when a player joins, and handles if a player is new or is a returning player.
            var channel = bot.GetChannel(480746606141112361) as SocketTextChannel;
            var result = Database.CheckExistingUser(user);
            Console.Write($"{ DateTime.Now} [DATABASE] A user has joined the discord. We will not check for them in the database. USER: " + user.Username + ".\n");
            if (result.Count <= 0)
            {
                //User is not in the database, and will now be entered into the database.
                Database.EnterUserMainTable(user);
                Database.EnterUserNoteTable(user);
                Database.EnterUserUserdataTable(user);
                //These are the minimum database entries to properly add a user to it.
                Console.Write($"{ DateTime.Now} [DATABASE] USER: " + user.Username + " was not in the database, and is now entered into the database.\n");
                await channel.SendMessageAsync($" **User:** {user.Username} has joined the server, <@&476471426245853198> please verify this user.\n");
            }
            else
            {
                //User is already in the database, so just assign role and return message
                Console.Write($"{ DateTime.Now} [DATABASE] USER: " + user.Username + " is a returning player, and will not be entered back into the database.\n");
                await channel.SendMessageAsync($" **User:** {user.Mention} \n**Time**: {DateTime.UtcNow}: Welcome back to the Discord server. Here's our return string.\n");
            }
        }
        public async Task HandleAdminLeave(SocketGuildUser user)
        {
            //This method will alert the console when a staff member leaves
            var channel = bot.GetChannel(480746606141112361) as SocketTextChannel;
            var result = Database.CheckExistingAdmin(user);
            Console.Write($"{ DateTime.Now} [DATABASE] An admin has left the discord. We will now check for them in the database. USER: " + user.Username + ".\n");
            if (result.Count <= 0)
            {
                //User is not in the database, and will now be entered into the database.
                Console.Write($"{ DateTime.Now} [DATABASE] USER: " + user.Username + " was not in the database, so no need to remove them..\n");
            }
            else
            {
                //Admin is in the database, so just need to remove them.
                Console.Write($"{ DateTime.Now} [DATABASE] USER: " + user.Username + " is an admin user, and will now be removed.\n");
                Database.RemoveUserAdminTable(user);
            }
            await channel.SendMessageAsync("A staff member left the server, and has been removed from the database.");
        }
        public async Task ConfigureAsync()
        {
            //await commands.AddModulesAsync(Assembly.GetEntryAssembly());
            await commands.AddModulesAsync(Assembly.GetEntryAssembly(), null);
        }
        public async Task HandleCommand(SocketMessage pMsg)
        {
            var message = pMsg as SocketUserMessage;
            int argPos = 1;
            var context = new CommandContext(bot, message);
            var result = await commands.ExecuteAsync(context, argPos, map);
            var channel = bot.GetChannel(560953282852421643) as SocketTextChannel;
            if (!(message.HasStringPrefix("-", ref argPos)) || message.Author.IsBot || message == null)
            {
                return;
            }
            if ((message.HasStringPrefix("-admin", ref argPos)) && result.IsSuccess)
            {
               Console.Write($"{ DateTime.Now} [ADMIN COMMAND] Admin User: " + context.User.Username + ", with command: " + message + "\n");
            }
            else if (result.IsSuccess && message.HasStringPrefix("-", ref argPos))
            {
                Console.Write($"{ DateTime.Now} [PLAYER COMMAND] User: " + context.User.Username + ", with command: " + message + "\n");
            }
        }

        public async Task HandleBadWords(SocketMessage pMsg)
        {
            var message = pMsg as SocketUserMessage;
            var context = new CommandContext(bot, message);
            var channel = bot.GetChannel(567074661179981834) as SocketTextChannel;
            var user = context.User;
            int argPos = 1;
            var lowerMessage = message.Content.ToLower();

            if (message.Author.IsBot || message.HasStringPrefix("-", ref argPos))
            {
                return;
            }

            if (message.Channel.Name == "admin" || message.Channel.Name == "internal-affairs" || message.Channel.Name.Contains("staff"))
            {
                return;
            }
            
            //Bad word/good word listening and reporting
            string[] badWords = {
                "fuck",
                "fucker",
                "fucking",
                "shit",
                "ass",
                "asshole",
                "assholes",
                "dick",
                "dicks",
                "nigger",
                "niggers",
                "niger",
                "nigers",
                "slut",
                "sluts",
                "bitch",
                "cunt",
                "rape",
                "queer"
            };
            string[] split = lowerMessage.Split(' ');
            if (badWords.Any(split.Contains))
            {
                await channel.SendMessageAsync("**User:** " + user.Username + " **In Channel**: " + message.Channel + " **Said**: " + message.Content);
            }
        }

        private async Task TalkToMe(SocketMessage pMsg)
        {
            var message = pMsg as SocketUserMessage;
            var context = new CommandContext(bot, message);
            var user = context.User;
            string[] startMaxwellPhrases = {
                "Maxwell",
                "maxwell"
            };
            string[] greetingPhrases =
            {
                "hi",
                "hello",
                "greetings",
                "hey",
                "sup",
                "howdy",
                "whats up"
            };
            string[] helpMe =
            {
                "help",
                "Help",
                "issue",
                "Issue",
                "Problem",
                "problem"

            };
            string[] launcherHelp =
            {
                "launcher",
                "Launcher"
            };
            string[] forumHelp =
            {
                "Forum",
                "forum"
            };
            string[] tcgHelp =
            {
                "TCG",
                "tcg",
                "Trading Card Game",
                "Trading card game",
                "trading card game"
            };
            string[] predictionsTexts = new string[]
            {
                "Hey there " + user.Username,
                "Oh it's " + user.Username,
                "HELLOW " + user.Username.ToUpper() + " take a :lollipop:",
                "Hi.",
                "Hey hey!"
            };
            string[] predictionsAnswer = new string[]
            {
                "Just no.",
                "Actually I don't think that is even a fair question to ask. Thanks for your effort though " + user.Username,
                "I don't understand, maybe ask someone stupid.",
                user.Username + " you're looking for me to answer that? Here's a :lollipop: bye now.",
                "I'm a busy bot, let me get back to the real work...",
                "Yeah I can see why **YOU'D** ask me that.",
                "Great question.",
                "You should do that.",
                "When the sun sets in the east, you'll find your answer.",
                "I could answer your question, but I'd have to mute you forever.",
                "Dude just build a wall."
            };
            var task = Task.Run(async () =>
            {
                var maxName = startMaxwellPhrases.Any(message.Content.Contains);
                var greetMax = greetingPhrases.Any(message.Content.Contains);
                var helpPlayer = helpMe.Any(message.Content.Contains);
                var probLaun = launcherHelp.Any(message.Content.Contains);
                var probForu = forumHelp.Any(message.Content.Contains);
                var probTcg = tcgHelp.Any(message.Content.Contains);
                Random rand = new Random();
                Random rand2 = new Random();
                int randomIndex = rand.Next(predictionsTexts.Length);
                int randomIndex2 = rand2.Next(predictionsAnswer.Length);
                string text = predictionsTexts[randomIndex];
                string text2 = predictionsAnswer[randomIndex2];

                if (message.Author.IsBot)
                {
                    return;
                }

                if (message.Content.EndsWith("?") && !helpPlayer && maxName)
                {
                    await context.Channel.SendMessageAsync(text2);
                }
                else if (maxName && greetMax && !helpPlayer)
                {
                    await context.Channel.SendMessageAsync(text);
                }
                else if (maxName && helpPlayer)
                {
                    if (probLaun)
                    {
                        await context.Channel.SendMessageAsync("[HELPING] " + user.Username + " I understand you have an issue with your launcher? You should tag a CSR member to get help.");
                    }
                    else if (probForu)
                    {
                        await context.Channel.SendMessageAsync("[HELPING] " + user.Username + " I understand you have an issue with the forums? You should tag a CSR member to get some help.");
                    }
                    else if (probTcg)
                    {
                        await context.Channel.SendMessageAsync("[HELPING] " + user.Username + " I understand you have an issue with the TCG (Trading Card Game)? You should tag a CSR member to get some help.");
                    }
                }
            });
        }
    }
}